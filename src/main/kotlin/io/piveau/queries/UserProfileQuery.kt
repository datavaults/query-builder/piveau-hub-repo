package io.piveau.queries

import io.piveau.ontologies.DVDM
import io.piveau.vocabularies.Countries
import io.piveau.vocabularies.Places
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import org.apache.jena.graph.NodeFactory
import org.apache.jena.graph.Triple
import org.apache.jena.sparql.core.Var
import org.apache.jena.sparql.expr.*
import org.apache.jena.sparql.syntax.ElementFilter
import org.apache.jena.sparql.syntax.ElementGroup
import org.apache.jena.sparql.vocabulary.FOAF
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

val dateFormatter: DateTimeFormatter = DateTimeFormatter.ISO_DATE.withZone(ZoneId.systemDefault())

fun Long.dateOfBirth(reference: Instant = Instant.now()): Instant = reference.minus(this, ChronoUnit.YEARS)

fun Instant.asDateNodeValue(): NodeValue = NodeValue.makeDate(dateFormatter.format(this))

fun buildUserProfileQuery(userProfileJson: JsonObject, catalogueVar: Var, catalogueGroup: ElementGroup): Boolean {
    var modified = false

    val publisherVar = Var.alloc("publisher")
    val publisherTriple = Triple.create(catalogueVar, DCTerms.publisher.asNode(), publisherVar)
    var publisherAdded = false

    val contactPointVar = Var.alloc("contactPoint")
    val contactPointTriple = Triple.create(catalogueVar, DCAT.contactPoint.asNode(), contactPointVar)
    var contactPointAdded = false

    userProfileJson.forEach { (property, value) ->
        when (property) {
            "countries" -> value.asResourceList(Countries).ifNotEmpty {
                catalogueGroup.addValueList(
                    this,
                    catalogueVar,
                    "country",
                    DCTerms.spatial.asNode()
                )
                modified = true
            }

            // PERSON foaf:gender (literal)
            "genders" -> {
                if (!publisherAdded) {
                    catalogueGroup.addTriplePattern(publisherTriple)
                    publisherAdded = true
                }
                val genderTriple =
                    Triple.create(publisherVar, FOAF.gender.asNode(), NodeFactory.createLiteral(value as String))
                catalogueGroup.addTriplePattern(genderTriple)
                modified = true
            }

            "cities" -> value.asResourceList(Places).ifNotEmpty {
                catalogueGroup.addValueList(
                    this,
                    catalogueVar,
                    "city",
                    DVDM.city.asNode()
                )
                modified = true
            }

            "regions" -> {
                catalogueGroup.addValueList(
                    value.asLiteralStringList(),
                    catalogueVar,
                    "region",
                    DVDM.region.asNode()
                )
                modified = true
            }

            // PERSON dvdm:nationality (concept)
            "nationalities" -> {
                if (!publisherAdded) {
                    catalogueGroup.addTriplePattern(publisherTriple)
                    publisherAdded = true
                }
                value.asResourceList(Countries).ifNotEmpty {
                    catalogueGroup.addValueList(
                        this,
                        publisherVar,
                        "nationality",
                        DVDM.nationality.asNode()
                    )
                    modified = true
                }
            }

            // PERSON foaf:birthday (literal date)
            "ages" -> {
                if (value is JsonArray && !value.isEmpty) {
                    if (!publisherAdded) {
                        catalogueGroup.addTriplePattern(publisherTriple)
                        publisherAdded = true
                    }

                    val ageFilter = createDateRangeFilter(value.map { it as JsonObject }, "birthday")
                    val birthdayTriple = Triple.create(publisherVar, FOAF.birthday.asNode(), Var.alloc("birthday"))
                    catalogueGroup.addTriplePattern(birthdayTriple)
                    catalogueGroup.addElementFilter(ageFilter)
                    modified = true
                }
            }

            // PERSON dvdm:placeOfBirth (concept)
            "placesOfBirth" -> {
                if (!publisherAdded) {
                    catalogueGroup.addTriplePattern(publisherTriple)
                    publisherAdded = true
                }
                value.asResourceList(Places).ifNotEmpty {
                    catalogueGroup.addValueList(
                        this,
                        publisherVar,
                        "placeOfBirth",
                        DVDM.placeOfBirth.asNode()
                    )
                    modified = true
                }
            }

            // INDIVIDUAL
            "postalCodes" -> {
                if (!contactPointAdded) {
                    catalogueGroup.addTriplePattern(contactPointTriple)
                    contactPointAdded = true
                }
            }

            // dvdm:occupation (literal)
            "occupations" -> catalogueGroup.addValueList(
                value.asLiteralIntegerList(),
                catalogueVar,
                "occupation",
                DVDM.occupation.asNode()
            ).also { modified = true }

            // dvdm:qualification (literal)
            "qualifications" -> catalogueGroup.addValueList(
                value.asLiteralStringList(),
                catalogueVar,
                "qualification",
                DVDM.qualification.asNode()
            ).also { modified = true }

            // dvdm:education (literal)
            "educations" -> catalogueGroup.addValueList(
                value.asLiteralIntegerList(),
                catalogueVar,
                "education",
                DVDM.education.asNode()
            ).also { modified = true }

            // dvdm:transportation (literal)
            "transportationMeans" -> catalogueGroup.addValueList(
                value.asLiteralIntegerList(),
                catalogueVar,
                "transportation",
                DVDM.transportation.asNode()
            ).also { modified = true }

            // dvdm:culturalInterest (literal)
            "culturalInterests" -> catalogueGroup.addValueList(
                value.asLiteralIntegerList(),
                catalogueVar,
                "culturalInterest",
                DVDM.culturalInterest.asNode()
            ).also { modified = true }

            // dvdm:civilStatus (literal)
            "civilStatus" -> catalogueGroup.addValueList(
                value.asLiteralStringList(),
                catalogueVar,
                "civilStatus",
                DVDM.civilStatus.asNode()
            ).also { modified = true }

            // dvdm:disabilities (literal)
            "disabilities" -> catalogueGroup.addValueList(
                value.asLiteralIntegerList(),
                catalogueVar,
                "disabilities",
                DVDM.disability.asNode()
            ).also { modified = true }

            // dvdm:nationalInsuranceNr (literal)
            "nationalInsuranceNumbers" -> catalogueGroup.addValueList(
                value.asLiteralStringList(),
                catalogueVar,
                "nationalInsuranceNr",
                DVDM.nationalInsuranceNr.asNode()
            ).also { modified = true }

            // dvdm:memberNumber (literal)
            "memberNumbers" -> catalogueGroup.addValueList(
                value.asLiteralStringList(),
                catalogueVar,
                "memberNumber",
                DVDM.memberNumber.asNode()
            ).also { modified = true }

            // dvdm:socialSecurityNumber (literal)
            "socialSecurityNumbers" -> catalogueGroup.addValueList(
                value.asLiteralStringList(),
                catalogueVar,
                "socialSecurityNumber",
                DVDM.socialSecurityNumber.asNode()
            ).also { modified = true }

            else -> {}
        }
    }
    return modified
}

fun createDateRangeFilter(defs: List<JsonObject>, variable: String): ElementFilter {
    val reference = Instant.now()

    val andExprs = defs
        .map {
            val min = it.getLong("min")
            val max = it.getLong("max")

            val minDateNode = max.dateOfBirth(reference).asDateNodeValue()
            val maxDateNode = min.dateOfBirth(reference).asDateNodeValue()

            val greaterExpr = E_GreaterThanOrEqual(
                minDateNode,
                ExprVar(variable)
            )

            val lessExpr = E_LessThanOrEqual(
                maxDateNode,
                ExprVar(variable)
            )

            E_LogicalAnd(greaterExpr, lessExpr)
        }

    val expr = if (andExprs.size > 1) {
        andExprs.first()
    } else {
        andExprs.first()
    }
    return ElementFilter(expr)
}
