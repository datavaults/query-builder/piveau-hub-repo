package io.piveau.queries

import io.vertx.core.json.JsonObject
import org.apache.jena.graph.Triple
import org.apache.jena.query.Query
import org.apache.jena.query.QueryFactory
import org.apache.jena.sparql.core.Var
import org.apache.jena.sparql.syntax.ElementGroup
import org.apache.jena.sparql.syntax.ElementNamedGraph
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.RDF

fun buildSparql(queryJson: JsonObject): Query {

    val userProfileJson = (queryJson.remove("userProfile") ?: JsonObject()) as JsonObject

    if (queryJson.isEmpty && userProfileJson.isEmpty) {
        return QueryFactory.create()
    }

    val query = QueryFactory.create()
    query.setQuerySelectType()

    val catalogueVar = Var.alloc("catalogue")
    query.addResultVar(catalogueVar.varName)

    val datasetVar = Var.alloc("dataset")
    query.addResultVar(datasetVar.varName)

    val rootGroup = ElementGroup()

    query.queryPattern = rootGroup

    val datasetGroup = ElementGroup()
    with(datasetGroup) {
        addTriplePattern(
            Triple.create(datasetVar, RDF.type.asNode(), DCAT.Dataset.asNode())
        )
    }
    rootGroup.addElement(ElementNamedGraph(datasetVar, datasetGroup))

    val catalogueGroup = ElementGroup()
    with(catalogueGroup) {
        addTriplePattern(
            Triple.create(catalogueVar, RDF.type.asNode(), DCAT.Catalog.asNode())
        )
        addTriplePattern(
            Triple.create(catalogueVar, DCAT.dataset.asNode(), datasetVar)
        )
    }
    rootGroup.addElement(ElementNamedGraph(catalogueVar, catalogueGroup))

    // Template ready

    val datasetQueryEmpty = buildDatasetQuery(queryJson, datasetVar, datasetGroup)
    val userProfileQueryEmpty = buildUserProfileQuery(userProfileJson, catalogueVar, catalogueGroup)
    return if (datasetQueryEmpty || userProfileQueryEmpty) {
        query
    } else {
        Query()
    }
}
