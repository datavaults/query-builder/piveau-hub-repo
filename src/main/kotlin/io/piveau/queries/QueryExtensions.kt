package io.piveau.queries

import io.piveau.vocabularies.ConceptScheme
import io.vertx.core.json.JsonArray
import org.apache.jena.datatypes.xsd.XSDDatatype
import org.apache.jena.graph.Node
import org.apache.jena.graph.NodeFactory
import org.apache.jena.graph.Triple
import org.apache.jena.query.Query
import org.apache.jena.sparql.core.Var
import org.apache.jena.sparql.engine.binding.BindingFactory
import org.apache.jena.sparql.syntax.ElementData
import org.apache.jena.sparql.syntax.ElementGroup

fun Query.straighten() = serialize().replace("\\s+".toRegex(), " ").trim()

fun Any.asResourceList(scheme: ConceptScheme) = (this as JsonArray).map(Any::toString).mapNotNull {
    scheme.fromLabel(it)?.resource?.asNode()
}

fun Any.asLiteralStringList() = (this as JsonArray).map(Any::toString).map { NodeFactory.createLiteral(it) }

fun Any.asLiteralIntegerList() = (this as JsonArray).map { it as Int }.map { NodeFactory.createLiteralByValue(it, XSDDatatype.XSDint) }

fun List<Node>.ifNotEmpty(body: List<Node>.() -> Unit) = if (isNotEmpty()) body() else Unit

fun ElementGroup.addValueList(value: List<Node>, subjectVar: Var, varName: String, predicateNode: Node) {
    val objectVar = Var.alloc(varName)
    val triple = Triple.create(subjectVar, predicateNode, objectVar)
    addTriplePattern(triple)
    val data = ElementData().apply {
        add(objectVar)
        value.forEach { node -> add(BindingFactory.binding(objectVar, node)) }
    }
    addElement(data)
}
