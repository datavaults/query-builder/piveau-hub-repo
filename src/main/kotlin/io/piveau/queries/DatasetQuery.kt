package io.piveau.queries

import io.piveau.ontologies.DVDM
import io.piveau.vocabularies.License
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import org.apache.jena.datatypes.xsd.XSDDatatype
import org.apache.jena.graph.NodeFactory
import org.apache.jena.graph.Triple
import org.apache.jena.sparql.core.Var
import org.apache.jena.sparql.expr.*
import org.apache.jena.sparql.expr.nodevalue.NodeValueDouble
import org.apache.jena.sparql.expr.nodevalue.NodeValueString
import org.apache.jena.sparql.syntax.ElementFilter
import org.apache.jena.sparql.syntax.ElementGroup
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms

fun buildDatasetQuery(queryJson: JsonObject, datasetVar: Var, datasetGroup: ElementGroup): Boolean {
    var modified = false
    queryJson.forEach { (key, value) ->
        when (key) {
            "category" -> datasetGroup.addValueList(
                value.asLiteralStringList(),
                datasetVar,
                "category",
                DVDM.category.asNode()
            ).also { modified = true }

            "name" -> {
                val filter = createTextFilter(value as String, "title")
                val titleTriple =
                    Triple.create(datasetVar, DCTerms.title.asNode(), Var.alloc("title"))
                datasetGroup.addTriplePattern(titleTriple)
                datasetGroup.addElementFilter(filter)
                modified = true
            }

            "description" -> {
                val filter = createTextFilter(value as String, "description")
                val descriptionTriple =
                    Triple.create(datasetVar, DCTerms.description.asNode(), Var.alloc("description"))
                datasetGroup.addTriplePattern(descriptionTriple)
                datasetGroup.addElementFilter(filter)
                modified = true
            }

            "keywords" -> datasetGroup.addValueList(
                value.asLiteralStringList(),
                datasetVar,
                "keyword",
                DCAT.keyword.asNode()
            ).also { modified = true }

            "license" -> (value as JsonArray).map(Any::toString).mapNotNull {
                License.getConcept(it)?.resource?.asNode()
            }.ifNotEmpty {
                datasetGroup.addValueList(
                    this,
                    datasetVar,
                    "license",
                    DCTerms.license.asNode()
                )
                modified = true
            }

            "price" -> {
                if (value is JsonObject) {
                    val filter = createRangeFilter(listOf(value), "price")

                    val priceTriple = Triple.create(datasetVar, DVDM.price.asNode(), Var.alloc("price"))

                    datasetGroup.addTriplePattern(priceTriple)
                    datasetGroup.addElementFilter(filter)

                    modified = true
                }
            }

            "anonymised" -> {
                val anonymisedTriple = Triple.create(
                    datasetVar,
                    DVDM.anonymised.asNode(),
                    NodeFactory.createLiteralByValue(value as Boolean, XSDDatatype.XSDboolean)
                )
                datasetGroup.addTriplePattern(anonymisedTriple)
                modified = true
            }

            "encrypted" -> {
                val encryptedTriple = Triple.create(
                    datasetVar,
                    DVDM.encrypted.asNode(),
                    NodeFactory.createLiteralByValue(value as Boolean, XSDDatatype.XSDboolean)
                )
                datasetGroup.addTriplePattern(encryptedTriple)
                modified = true
            }

            "useTPM" -> {
                val useTPMTriple = Triple.create(
                    datasetVar,
                    DVDM.useTPM.asNode(),
                    NodeFactory.createLiteralByValue(value as Boolean, XSDDatatype.XSDboolean)
                )
                datasetGroup.addTriplePattern(useTPMTriple)
                modified = true
            }

            "source" -> datasetGroup.addValueList(
                value.asLiteralStringList(),
                datasetVar,
                "source",
                DCTerms.source.asNode()
            ).also { modified = true }

            else -> {}
        }
    }
    return modified
}

fun createRangeFilter(defs: List<JsonObject>, variable: String): ElementFilter {
    val exprs = defs.map {
        val min = it.getDouble("min")
        val max = it.getDouble("max")

        val greater = E_GreaterThanOrEqual(ExprVar(variable), NodeValueDouble(min))
        val less = E_LessThanOrEqual(ExprVar(variable), NodeValueDouble(max))
        E_LogicalAnd(greater, less)
    }

    val expr = if (exprs.size > 1) {
        var or = E_LogicalOr(exprs[0], exprs[1])
        for (i in 3 .. exprs.size) {
            or = E_LogicalOr(or, exprs[i - 1])
        }
        or
    } else {
        exprs.first()
    }
    return ElementFilter(expr)
}

fun createTextFilter(text: String, variable: String): ElementFilter {
    val lowerExpr = E_StrLowerCase(ExprVar(variable))
    val containsExpr = E_StrContains(lowerExpr, NodeValueString(text.lowercase()))
    return ElementFilter(containsExpr)
}