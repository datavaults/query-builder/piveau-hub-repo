package io.piveau.hub.shell

import io.piveau.hub.services.index.IndexService
import io.piveau.vocabularies.*
import io.vertx.core.Vertx
import io.vertx.core.cli.Argument
import io.vertx.core.cli.CLI
import io.vertx.core.cli.Option
import io.vertx.core.json.JsonObject
import io.vertx.ext.shell.command.Command
import io.vertx.ext.shell.command.CommandBuilder
import io.vertx.ext.shell.command.CommandProcess

val vocabularies = mapOf(
    DataTheme.id to DataTheme,
    Continents.id to Continents,
    Countries.id to Countries,
    Places.id to Places,
    CorporateBodies.id to CorporateBodies,
    Frequency.id to Frequency,
    License.id to License,
    Languages.id to Languages
)

class VocabularyCommand private constructor(vertx: Vertx) {
    private val indexService: IndexService = IndexService.createProxy(vertx, IndexService.SERVICE_ADDRESS)
    private val command: Command

    init {
        val config = vertx.orCreateContext.config()
        command = CommandBuilder.command(
            CLI.create("vocabs")
                .addOption(
                    Option().setHelp(true).setFlag(true).setArgName("help").setShortName("h").setLongName("help")
                )
                .addArgument(Argument().setRequired(true).setIndex(1).setArgName("action").setDescription("Possible actions are 'list', 'show', or 'indexing'."))
                .addArgument(Argument().setRequired(false).setArgName("vocabs").setMultiValued(true).setDescription("Specify one or more vocabularies for the 'show' or 'indexing' action."))
        ).processHandler { process ->
            val commandLine = process.commandLine()
            when (process.commandLine().getArgumentValue<String>(1)) {
                "list" -> process.write("${vocabularies.keys.joinToString("\n")}\n").end()
                "indexing" -> {
                    val vocabs = commandLine.getArgumentValues<String>(2)
                    when {
                        vocabs.isEmpty() -> {
                            vocabularies.map { Pair(it.key, indexingSKOSVocabulary(it.value)) }.forEach {
                                send(process, indexingSKOSVocabulary(vocabularies.getValue(it.first)))
                            }
                            process.end()
                        }
                        else -> {
                            vocabs.forEach {
                                if (vocabularies.containsKey(it)) {
                                    send(process, indexingSKOSVocabulary(vocabularies.getValue(it)))
                                } else {
                                    process.write("Unknown vocabulary $it.\n")
                                }
                            }
                            process.end()
                        }
                    }
                }
                "show" -> {
                    val vocabs = commandLine.getArgumentValues<String>(2)
                    when {
                        vocabs.isEmpty() -> {
                            vocabularies.forEach {
                                process.write("${indexingSKOSVocabulary(it.value).encodePrettily()}\n")
                            }
                        }
                        else -> {
                            vocabs.forEach {
                                if (vocabularies.containsKey(it)) {
                                    process.write("${indexingSKOSVocabulary(vocabularies.getValue(it)).encodePrettily()}\n")
                                } else {
                                    process.write("Unknown vocabulary $it.\n")
                                }
                            }
                        }
                    }
                    process.end()
                }
                else -> {
                    process.write("Unknown action.\n").end()
                }
            }
        }.build(vertx)
    }

    private fun send(process: CommandProcess, index: JsonObject) {
        indexService.putVocabulary(index) { ar ->
            if (ar.succeeded()) {
                process.write("${index.getString("id")} indexed successfully\n")
            } else {
                process.write("Indexing ${index.getString("id")} failed: ${ar.cause().message}\n")
            }
        }
    }

    companion object {
        fun create(vertx: Vertx): Command {
            return VocabularyCommand(vertx).command
        }
    }
}
