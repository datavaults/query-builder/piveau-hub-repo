package io.piveau.hub.shell

import io.piveau.dcatap.DCATAPUriSchema
import io.piveau.dcatap.TripleStore
import io.piveau.hub.util.Constants
import io.piveau.json.asJsonObject
import io.vertx.core.Vertx
import io.vertx.core.cli.Argument
import io.vertx.core.cli.CLI
import io.vertx.core.cli.Option
import io.vertx.ext.shell.command.Command
import io.vertx.ext.shell.command.CommandBuilder
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.coroutines.await
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.apache.jena.vocabulary.DCAT

class RepairCatalogueCommand private constructor(vertx: Vertx) {
    private val command: Command
    private val tripleStore: TripleStore

    init {
        val config = vertx.orCreateContext.config()
        tripleStore = TripleStore(
            vertx,
            config.asJsonObject(Constants.ENV_PIVEAU_TRIPLESTORE_CONFIG),
            WebClient.create(vertx)
        )

        command = CommandBuilder.command(
            CLI.create("repair")
                .addArgument(
                    Argument()
                        .setArgName("catalogueId")
                        .setRequired(true)
                        .setDescription("The id of the catalogue")
                )
                .addOption(
                    Option().setHelp(true).setFlag(true).setArgName("help").setShortName("h").setLongName("help")
                        .setDescription("This help")
                )
        ).completionHandler { completion ->
            val input = completion.rawLine().trim()
            tripleStore.catalogueManager.listUris().onSuccess { list ->
                val ids = list
                    .map { it.id }
                    .filter { id -> id.startsWith(input) }
                    .toList()
                when (ids.size) {
                    0 -> completion.complete("", false)
                    1 -> completion.complete(ids[0].removePrefix(input), true)
                    else -> completion.complete(ids)
                }
            }.onFailure { completion.complete("", true) }
        }.processHandler { process ->
            GlobalScope.launch(Dispatchers.IO) {
                val catalogueId = process.commandLine().getArgumentValue<String>(0)
                val catalogueSchema = DCATAPUriSchema.applyFor(catalogueId)
                val datasets = tripleStore.catalogueManager.allDatasets(catalogueId).await()
                process.write("Found ${datasets.size} dataset entries in catalogue\n")
                datasets.forEach { dataset ->
                    try {
                        when {
                            tripleStore.datasetManager.existGraph(dataset.uriRef).await() -> {
                                tripleStore.catalogueManager.addDatasetEntry(
                                    catalogueSchema.catalogueGraphName,
                                    catalogueSchema.catalogueUriRef,
                                    dataset.datasetUriRef,
                                    dataset.recordUriRef
                                )
                            }
                            else -> {
                                tripleStore.catalogueManager.removeDatasetEntry(catalogueSchema.catalogueUriRef, dataset.uriRef)
                                    .await()
                            }
                        }
                    } catch (e: Exception) {
                        process.write("Cleaning dataset entry $dataset failed: ${e.message}\n")
                    }
                }

                val records = tripleStore.catalogueManager.allRecords(catalogueId).await()
                process.write("Found ${records.size} record entries in catalogue\n")
                val obsolete = records - datasets.map { it.recordUriRef }
                obsolete
//                records
//                    .map { DCATAPUriSchema.parseUriRef(it) }
//                    .filterNot { datasets.contains(it.datasetUriRef) }
                    .forEach {
                        try {
                            tripleStore.update(
                                "DELETE DATA { GRAPH <${catalogueSchema.catalogueGraphName}> { <${catalogueSchema.catalogueUriRef}> <${DCAT.record}> <$it> } }"
                            ).await()
                        } catch (e: Exception) {
                            process.write("Delete $it failed: ${e.message}\n")
                        }
                    }

                process.write("Repair finished\n").end()
            }
        }.build(vertx)
    }

    companion object {
        fun create(vertx: Vertx): Command {
            return RepairCatalogueCommand(vertx).command
        }
    }

}