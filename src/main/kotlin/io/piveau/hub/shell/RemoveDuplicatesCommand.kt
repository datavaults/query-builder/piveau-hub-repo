package io.piveau.hub.shell

import io.piveau.dcatap.DCATAPUriSchema
import io.piveau.dcatap.TripleStore
import io.piveau.hub.services.index.IndexService
import io.piveau.hub.util.Constants
import io.piveau.json.asJsonObject
import io.vertx.core.Vertx
import io.vertx.core.cli.Argument
import io.vertx.core.cli.CLI
import io.vertx.core.cli.Option
import io.vertx.ext.shell.command.Command
import io.vertx.ext.shell.command.CommandBuilder
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.coroutines.await
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import org.apache.jena.sparql.vocabulary.FOAF
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms

class RemoveDuplicatesCommand private constructor(vertx: Vertx) {
    private val indexService: IndexService = IndexService.createProxy(vertx, IndexService.SERVICE_ADDRESS)
    private val tripleStore: TripleStore
    private val command: Command

    init {
        val config = vertx.orCreateContext.config()
        tripleStore = TripleStore(
            vertx,
            config.asJsonObject(Constants.ENV_PIVEAU_TRIPLESTORE_CONFIG),
            WebClient.create(vertx)
        )
        command = CommandBuilder.command(
            CLI.create("repairDups")
                .addArgument(
                    Argument()
                        .setArgName("catalogueId")
                        .setRequired(true)
                        .setDescription("The id of the catalogue")
                )
                .addOption(
                    Option().setHelp(true).setFlag(true).setArgName("help").setShortName("h").setLongName("help").setDescription("This help")
                )
                .addOption(
                    Option().setFlag(true).setArgName("verbose").setShortName("v").setLongName("verbose").setDescription("Verbose output")
                )
        ).completionHandler { completion ->
            val input = completion.rawLine().trim()
            tripleStore.catalogueManager.listUris().onSuccess { list ->
                val ids = list
                    .map { it.id }
                    .filter { id -> id.startsWith(input) }
                    .toList()
                when (ids.size) {
                    0 -> completion.complete("", false)
                    1 -> completion.complete(ids[0].removePrefix(input), true)
                    else -> completion.complete(ids)
                }
            }.onFailure { completion.complete("", true) }
        }.processHandler { process ->
            val catalogueId = process.commandLine().getArgumentValue<String>(0)
            val catalogueSchema = DCATAPUriSchema.applyFor(catalogueId)
            tripleStore.catalogueManager.allDatasetIdentifiers(catalogueId)
                .onSuccess { identifiers ->
                    process.write("Found ${identifiers.size} identifiers\n")
                    GlobalScope.launch(Dispatchers.IO) {
                        identifiers.asFlow()
                            .transform { id ->
                                if (process.commandLine().isFlagEnabled("verbose")) {
                                    process.write("Checking $id...\n")
                                }
                                val query = """SELECT ?d WHERE { GRAPH <${catalogueSchema.catalogueGraphName}> { ?c <${DCAT.record}> ?r } GRAPH ?g { ?r <${DCTerms.identifier}> "$id" ; <${FOAF.primaryTopic}> ?d } }"""
                                val resultSet = tripleStore.select(query).await()
                                val duplicates = resultSet.asSequence().map { it.getResource("d").uri }.toList()
                                if (duplicates.size > 1) {
                                    process.write("$id exists ${duplicates.size} times. keeping only one, deleting the rest.\n")
                                    emit(duplicates)
                                } else if (process.commandLine().isFlagEnabled("verbose")) {
                                    process.write("No duplicates for $id\n")
                                }
                            }.transform { duplicates ->
                                process.write("Duplicates: ${duplicates.joinToString(",")}\n")
                                duplicates
                                    .filter { it.matches(""".+_\d+$""".toRegex()) }
                                    .forEach { dataset ->
                                        val datasetSchema = DCATAPUriSchema.parseUriRef(dataset)
                                        // deleting here
                                        tripleStore.deleteGraph(datasetSchema.datasetGraphName).onFailure {
                                            process.write("Delete dataset graph failure: ${it.message}\n")
                                        }
                                        tripleStore.deleteGraph(datasetSchema.metricsGraphName).onFailure {
                                            process.write("Delete metrics graph failure: ${it.message}\n")
                                        }
                                        tripleStore.deleteGraph(datasetSchema.historicMetricsGraphName).onFailure {
                                            process.write("Delete metrics history graph failure: ${it.message}\n")
                                        }

                                        indexService.deleteDataset(datasetSchema.id) {
                                            if (it.failed()) {
                                                process.write("Remove dataset from index failure: ${it.cause().message}\n")
                                            }
                                        }

                                        tripleStore.catalogueManager
                                            .removeDatasetEntry(catalogueSchema.catalogueUriRef, datasetSchema.datasetUriRef).onFailure {
                                                process.write("Remove entries from catalogue failure: ${it.message}\n")
                                            }

                                        process.write("Deleted ${datasetSchema.id}\n")

                                        emit(dataset)
                                    }
                            }
                            .onCompletion {
                                it?.also { process.write("${it.cause?.message}\n").end() }
                                    ?: process.write("Repair finished\n").end()
                            }
                            .collect { dataset ->
                                process.write("Dataset $dataset duplicates cleared\n")
                            }
                    }
                }
                .onFailure { cause ->
                    process.write("${cause.message}\n").end()
                }
        }.build(vertx)
    }

    companion object {
        fun create(vertx: Vertx): Command {
            return RemoveDuplicatesCommand(vertx).command
        }
    }

}