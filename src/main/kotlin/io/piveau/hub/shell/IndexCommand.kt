package io.piveau.hub.shell

import io.piveau.dcatap.DCATAPUriSchema
import io.piveau.dcatap.TripleStore
import io.piveau.hub.services.index.IndexService
import io.piveau.hub.util.Constants
import io.piveau.indexing.indexingCatalogue
import io.piveau.indexing.indexingDataset
import io.piveau.json.asJsonObject
import io.piveau.vocabularies.Languages
import io.vertx.core.*
import io.vertx.core.cli.Argument
import io.vertx.core.cli.CLI
import io.vertx.core.cli.Option
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.shell.command.Command
import io.vertx.ext.shell.command.CommandBuilder
import io.vertx.ext.shell.command.CommandProcess
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.coroutines.await
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.apache.jena.vocabulary.DCTerms
import java.util.concurrent.atomic.AtomicInteger
import java.util.stream.Collectors
import kotlin.streams.toList
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

@ExperimentalTime
class IndexCommand private constructor(vertx: Vertx) {
    private val command: Command
    private val tripleStore: TripleStore
    private val indexService: IndexService = IndexService.createProxy(vertx, IndexService.SERVICE_ADDRESS)
    private val client: WebClient = WebClient.create(vertx)

    private val elasticsearchAddress: String = vertx.orCreateContext.config()
        .getString(Constants.ENV_PIVEAU_HUB_ELASTICSEARCH_ADDRESS, "http://elasticsearch:9200")

    init {
        val config = vertx.orCreateContext.config()
        tripleStore = TripleStore(
            vertx,
            config.asJsonObject(Constants.ENV_PIVEAU_TRIPLESTORE_CONFIG),
            WebClient.create(vertx)
        )
        command = CommandBuilder.command(
            CLI.create("index")
                .addArgument(
                    Argument()
                        .setArgName("catalogueIds")
                        .setRequired(false)
                        .setMultiValued(true)
                        .setDescription("The ids of the catalogues to index")
                )
                .addOption(
                    Option().setArgName("help")
                        .setHelp(true)
                        .setFlag(true)
                        .setShortName("h")
                        .setLongName("help")
                        .setDescription("This help")
                )
                .addOption(
                    Option().setArgName("chunkSize")
                        .setRequired(false)
                        .setArgName("chunkSize")
                        .setDefaultValue("500")
                        .setShortName("c")
                        .setLongName("chunkSize")
                        .setDescription("The number of parallel requests")
                )
        ).completionHandler { completion ->
            val input = completion.lineTokens().last().value()
            tripleStore.catalogueManager.listUris().onSuccess { list ->
                val ids = when {
                    input.isBlank() -> list.map { it.id }.toList()
                    else -> list.map { it.id }
                        .filter { id -> id.startsWith(input) }
                        .toList()
                }
                when (ids.size) {
                    0 -> completion.complete("", false)
                    1 -> completion.complete(ids[0].removePrefix(input), list.size > 1)
                    else -> completion.complete(ids)
                }
            }.onFailure { completion.complete("", false) }
        }.processHandler { process ->
            GlobalScope.launch(Dispatchers.IO) {
                process.interruptHandler { process.end() }

                var catalogueIds = process.commandLine().allArguments()
                if (catalogueIds.isEmpty()) {
                    process.write("Indexing all catalogues\n")
                    val uris = tripleStore.catalogueManager.listUris().await()
                    catalogueIds = uris.map { it.id }
                }
                catalogueIds.forEach {
                    indexCatalogue(it, process).await()
                }
                if (catalogueIds.size > 1) {
                    process.write("Indexing of catalogues finished.\n")
                }
                process.end()
            }
        }.build(vertx)
    }

    private suspend fun indexCatalogue(catalogueId: String, process: CommandProcess) = Promise.promise<Void>().apply {
        process.write("Indexing catalogue $catalogueId\n")

        try {
            measureTime {
                val catalogueModel = tripleStore.catalogueManager.getStripped(catalogueId).await()
                val catalogueIndex =
                    indexingCatalogue(catalogueModel.getResource(DCATAPUriSchema.createForCatalogue(catalogueId).catalogueUriRef)).await()

                Promise.promise<JsonObject>().apply { indexService.addCatalog(catalogueIndex, this) }.future()
                    .await()

                process.write("Catalogue $catalogueId metadata indexed successfully\n")

                val lang = catalogueModel.listObjectsOfProperty(DCTerms.language).next().asResource()
                val defaultLang =
                    Languages.iso6391Code(Languages.getConcept(lang) ?: Languages.getConcept("ENG")!!) ?: "en"

                val datasets = tripleStore.catalogueManager.allDatasets(catalogueId).await()
                process.write("Indexing ${datasets.size} datasets\n")
                val counter = AtomicInteger(0)

                val chunkSize = process.commandLine().getOptionValue<String>("chunkSize")

                datasets.chunked(chunkSize.toInt()).forEach { chunk ->
                    val futures = mutableListOf<Future<JsonObject>>()
                    chunk.forEach {
                        futures.add(tripleStore.datasetManager.getGraph(it.uriRef).compose { model ->
                            Promise.promise<JsonObject>().apply {
                                indexService.addDatasetPut(
                                    indexingDataset(
                                        model.getResource(it.uriRef),
                                        model.getResource(it.recordUriRef),
                                        catalogueId,
                                        defaultLang
                                    ), this
                                )
                            }.future().onFailure { cause ->
                                process.write("Failure $it: ${cause.message}\n")
                            }
                        })
                    }
                    try {
                        CompositeFuture.join(futures as List<Future<Any>>).await()
                    } catch (e: Exception) {
                        process.write("Join failure: ${e.message}\n")
                    }

                    process.write("\rIndexed ${counter.addAndGet(chunk.size)}")
                }
                process.write("\nDatasets of $catalogueId indexed successfully\n")

                // remove obsolete indexed datasets
                val indexIds = getIndexDatasetIds(catalogueId).await()
                val datasetIds = datasets.stream().map { it.id }.toList()
                process.write("${indexIds.size} datasets in index, ${datasetIds.size} datasets in store\n")

                indexIds.minus(datasetIds).forEach {
                    Promise.promise<JsonObject>().apply { indexService.deleteDataset(it, this) }
                        .future()
                        .await()
                }
                process.write("Indexing of $catalogueId finished\n")
            }.apply {
                process.write("Duration: ${this.inSeconds} seconds\n")
                complete()
            }
        } catch (e: Exception) {
            process.write("Something went wrong: ${e.message}\n")
            fail(e)
        }
    }.future()

    private fun getIndexDatasetIds(catalogueId: String): Future<Set<String>> =
        Promise.promise<Set<String>>().apply {
            client.getAbs("$elasticsearchAddress/dataset/_search")
                .addQueryParam("q", "catalog.id:$catalogueId")
                .addQueryParam("_source_includes", "id")
                .addQueryParam("size", "350000")
                .send { ar ->
                    if (ar.succeeded()) {
                        val result = ar.result().bodyAsJsonObject()
                        val hits = result.getJsonObject("hits", JsonObject())
                            .getJsonArray("hits", JsonArray())
                        val ids = hits.stream()
                            .map { obj: Any ->
                                (obj as JsonObject).getJsonObject(
                                    "_source"
                                ).getString("id")
                            }
                            .collect(Collectors.toSet())
                        complete(ids)
                    } else {
                        fail(ar.cause())
                    }
                }
        }.future()

    companion object {
        fun create(vertx: Vertx): Command {
            return IndexCommand(vertx).command
        }
    }

}