package io.piveau.hub;

import io.piveau.hub.handler.*;
import io.piveau.hub.security.*;
import io.piveau.hub.services.DatasetHelperMessageCodec;
import io.piveau.hub.services.catalogues.CataloguesServiceVerticle;
import io.piveau.hub.services.datasets.DatasetsServiceVerticle;
import io.piveau.hub.services.distributions.DistributionsServiceVerticle;
import io.piveau.hub.services.index.IndexServiceVerticle;
import io.piveau.hub.services.metrics.MetricsServiceVerticle;
import io.piveau.hub.services.queries.QueriesServiceVerticle;
import io.piveau.hub.services.translation.TranslationServiceVerticle;
import io.piveau.hub.util.Constants;
import io.piveau.hub.dataobjects.DatasetHelper;
import io.piveau.hub.util.logger.PiveauLogger;
import io.piveau.hub.util.logger.PiveauLoggerFactory;
import io.piveau.hub.shell.ShellVerticle;
import io.piveau.json.ConfigHelper;
import io.piveau.dcatap.DCATAPUriSchema;
import io.piveau.pipe.PiveauCluster;
import io.piveau.security.ApiKeyAuthProvider;
import io.piveau.utils.ConfigurableAssetHandler;
import io.piveau.security.PiveauAuth;
import io.piveau.security.PiveauAuthConfig;
import io.piveau.vocabularies.ConceptSchemes;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.*;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.handler.APIKeyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.openapi.RouterBuilder;
import io.vertx.ext.web.openapi.RouterBuilderOptions;
import io.vertx.ext.web.validation.BadRequestException;

import java.util.*;

/**
 * This is the main entry point of the application
 */
public class MainVerticle extends AbstractVerticle {

    private CatalogueHandler catalogueHandler;

    private DatasetHandler datasetHandler;
    private MetricsHandler metricsHandler;
    private DistributionHandler distributionHandler;
    private IndexHandler indexHandler;
    private TranslationHandler translationHandler;
    //    private VocabularyHandler vocabularyHandler;
    private QueriesHandler queriesHandler;

    /**
     * Composes all function for starting the Main Verticle
     */
    @Override
    public void start(Promise<Void> startPromise) {
        PiveauLoggerFactory.getLogger(getClass()).info("Starting piveau hub...");
        loadConfig()
                .compose(this::bootstrapVerticles)
                .compose(this::startServer)
                .onComplete(startPromise);
    }

    /**
     * Loads the configuration file
     *
     * @return Configuration Object
     */
    private Future<JsonObject> loadConfig() {
        ConfigStoreOptions envStoreOptions = new ConfigStoreOptions()
                .setType("env");
        ConfigStoreOptions fileStoreOptions = new ConfigStoreOptions()
                .setType("file")
                .setConfig(new JsonObject().put("path", "conf/config.json"));

        ConfigRetriever retriever = ConfigRetriever
                .create(vertx, new ConfigRetrieverOptions()
                        .addStore(fileStoreOptions)
                        .addStore(envStoreOptions));

        return retriever.getConfig();
    }

    private Future<Void> startServer(JsonObject config) {
        Promise<Void> promise = Promise.promise();

        vertx.eventBus().registerDefaultCodec(DatasetHelper.class, new DatasetHelperMessageCodec());
        PiveauLogger.setBaseUri(config.getString(Constants.ENV_PIVEAU_HUB_BASE_URI, "https://piveau.io/"));
        PiveauLogger LOGGER = PiveauLoggerFactory.getLogger(getClass());

        JsonObject vocabulariesConfig = config.getJsonObject(Constants.ENV_PIVEAU_HUB_VOCABULARIES_CONFIG, new JsonObject());
        ConceptSchemes.initRemotes(
                vertx,
                vocabulariesConfig.getBoolean("remoteFetch", false),
                vocabulariesConfig.getBoolean("preload", true));

        DCATAPUriSchema.INSTANCE.setConfig(new JsonObject().put("baseUri", config.getString(Constants.ENV_PIVEAU_HUB_BASE_URI, "https://io.piveau/")));
        Integer port = config.getInteger(Constants.ENV_PIVEAU_HUB_SERVICE_PORT, 8080);

        String greeting = config.getString("greeting");

        RouterBuilder.create(vertx, "webroot/openapi.yaml")
                .onSuccess(builder -> {
                    RouterBuilderOptions options = new RouterBuilderOptions().setMountNotImplementedHandler(true).setOperationModelKey("assd");
                    builder.setOptions(options);

                    JsonArray corsDomains = ConfigHelper.forConfig(config).forceJsonObject(Constants.ENV_PIVEAU_HUB_CORS_CONFIG).getJsonArray("domains", new JsonArray());
                    if (!corsDomains.isEmpty()) {

                        Set<String> allowedHeaders = new HashSet<>();
                        allowedHeaders.add("x-requested-with");
                        allowedHeaders.add("Access-Control-Allow-Origin");
                        allowedHeaders.add("origin");
                        allowedHeaders.add("Content-Type");
                        allowedHeaders.add("Accept");
                        allowedHeaders.add("Authorization");

                        Set<HttpMethod> allowedMethods = new HashSet<>();
                        allowedMethods.add(HttpMethod.GET);
                        allowedMethods.add(HttpMethod.POST);
                        allowedMethods.add(HttpMethod.OPTIONS);
                        allowedMethods.add(HttpMethod.DELETE);
                        allowedMethods.add(HttpMethod.PATCH);
                        allowedMethods.add(HttpMethod.PUT);

                        ArrayList<String> corsArray = new ArrayList<>();
                        for (int i = 0; i < corsDomains.size(); i++) {
                            //convert into normal array and escape dots for regex compatibility
                            corsArray.add(corsDomains.getString(i).replace(".", "\\."));
                        }

                        //"^(https?:\\/\\/(?:.+\\.)?(?:fokus\\.fraunhofer\\.de|localhost)(?::\\d{1,5})?)$"
                        String corsString = "^(https?:\\/\\/(?:.+\\.)?(?:" + String.join("|", corsArray) + ")(?::\\d{1,5})?)$";

                        builder.rootHandler(CorsHandler.create(corsString).allowedHeaders(allowedHeaders).allowedMethods(allowedMethods).allowCredentials(true));
                    }

                    JsonObject authConfig = config.getJsonObject(Constants.ENV_PIVEAU_HUB_AUTH_CONFIG, new JsonObject());
                    String apiKey = authConfig.getString("apiKey", Constants.DEFAULT_API_KEY);
                    builder.securityHandler("ApiKey", APIKeyHandler.create(new ApiKeyAuthProvider(apiKey)).header("Authorization"));

                    PiveauAuth.create(vertx, new PiveauAuthConfig(authConfig))
                            .compose(piveauAuth -> {
                                builder.securityHandler("BearerAuth", piveauAuth.authHandler());

                                // listCatalogues
                                builder.operation("listCatalogues").handler(catalogueHandler::handleListCatalogues);
                                // getCatalogue
                                builder.operation("getCatalogue").handler(catalogueHandler::handleGetCatalogue);
                                // putCatalogue
                                builder.operation("putCatalogue")
                                        .handler(new CataloguePermissionHandler(List.of("create", "update")))
                                        .handler(catalogueHandler::handlePutCatalogue);
                                // deleteCatalogue
                                builder.operation("deleteCatalogue")
                                        .handler(new CataloguePermissionHandler(List.of("delete")))
                                        .handler(catalogueHandler::handleDeleteCatalogue);

                                // listCatalogueDatasets
                                builder.operation("listCatalogueDatasets").handler(datasetHandler::handleListCatalogueDatasets);
                                // postCatalogueDataset
                                // !?!
                                // putCatalogueDataset
                                builder.operation("putCatalogueDataset").handler(datasetHandler::handlePutDatasetNew);
                                // getCatalogueDatasetOrigin
                                // putCatalogueDatasetOrigin
                                // deleteCatalogueDataset
                                builder.operation("deleteCatalogueDataset").handler(datasetHandler::handleDeleteDatasetNew);

                                // listDatasets
                                builder.operation("listDatasets").handler(datasetHandler::handleListDatasets);
                                // getDataset
                                builder.operation("getDataset").handler(datasetHandler::handleGetDataset);
                                // putDataset
                                builder.operation("putDataset")
                                        .handler(new DatasetPermissionHandler(List.of("create", "update")))
                                        .handler(datasetHandler::handlePutDataset);
                                // deleteDataset
                                builder.operation("deleteDataset")
                                        .handler(new DatasetPermissionHandler(List.of("delete")))
                                        .handler(datasetHandler::handleDeleteDatasetNew);
                                // postDatasetDistribution
                                builder.operation("postDatasetDistribution")
                                        .handler(new DatasetPermissionHandler(List.of("update")))
                                        .handler(datasetHandler::handlePostDatasetDistribution);
                                // getDatasetRecord
                                builder.operation("getDatasetRecord").handler(datasetHandler::handleGetDatasetRecord);


                                // getDistribution
                                builder.operation("getDistribution").handler(distributionHandler::handleGetDistribution);
                                // putDistribution
                                builder.operation("putDistribution")
                                        .handler(new DatasetPermissionHandler(List.of("update")))
                                        .handler(distributionHandler::handlePutDistribution);
                                // deleteDistribution
                                builder.operation("deleteDistribution")
                                        .handler(new DatasetPermissionHandler(List.of("update")))
                                        .handler(distributionHandler::handleDeleteDistribution);

                                // getMetrics
                                builder.operation("getMetrics").handler(metricsHandler::handleGetMetrics);
                                // putMetrics
                                builder.operation("putMetrics")
                                        .handler(new DatasetPermissionHandler(List.of("update")))
                                        .handler(metricsHandler::handlePutMetrics);
                                // deleteMetrics
                                builder.operation("deleteMetrics")
                                        .handler(new DatasetPermissionHandler(List.of("update")))
                                        .handler(metricsHandler::handleDeleteMetrics);

                                // indexDataset
                                builder.operation("indexDataset")
                                        .handler(new DatasetPermissionHandler(List.of("update")))
                                        .handler(indexHandler::handleIndexDataset);

                                // postTranslation
                                builder.operation("postTranslation")
                                        .handler(new DatasetPermissionHandler(List.of("update")))
                                        .handler(translationHandler::handlePostTranslation);

                                Router router = builder.createRouter();

                                router.errorHandler(400, context -> {
                                    if (context.failure() instanceof BadRequestException) {
                                        BadRequestException br = (BadRequestException) context.failure();
                                        JsonObject error = new JsonObject()
                                                .put("status", "error")
                                                .put("message", br.getMessage());
                                        context.response().setStatusCode(400).putHeader("Content-Type", "application/json").end(error.encodePrettily());
                                    }
                                });

                                WebClient client = WebClient.create(vertx);

                                router.route("/*").handler(StaticHandler.create());
                                router.route("/info").handler(context -> healthHandler(context, greeting));
                                router.route("/images/logo").handler(new ConfigurableAssetHandler(config.getString(Constants.ENV_PIVEAU_LOGO_PATH, "webroot/images/logo.png"), client));
                                router.route("/images/favicon").handler(new ConfigurableAssetHandler(config.getString(Constants.ENV_PIVEAU_FAVICON_PATH, "webroot/images/favicon.png"), client));

                                RouterBuilder.create(vertx, "webroot/openapi-query-builder.yaml")
                                        .onSuccess(subRouterBuilder -> {
                                            RouterBuilderOptions subRouterOptions = new RouterBuilderOptions().setMountNotImplementedHandler(true);
                                            subRouterBuilder.setOptions(subRouterOptions);

                                            subRouterBuilder.securityHandler("ApiKey", APIKeyHandler.create(new ApiKeyAuthProvider(apiKey)).header("Authorization"));

                                            subRouterBuilder.operation("listQueries").handler(queriesHandler::listQueries);
                                            subRouterBuilder.operation("query").handler(queriesHandler::query);

                                            Router subRouter = subRouterBuilder.createRouter();
                                            subRouter.route("/images/logo").handler(new ConfigurableAssetHandler(config.getString(Constants.ENV_PIVEAU_LOGO_PATH, "webroot/images/logo.png"), client));
                                            subRouter.route("/images/favicon").handler(new ConfigurableAssetHandler(config.getString(Constants.ENV_PIVEAU_FAVICON_PATH, "webroot/images/favicon.png"), client));
                                            subRouter.route("/*").handler(StaticHandler.create().setIndexPage("index-query-builder.html"));

                                            router.mountSubRouter("/query-builder", subRouter);
                                        })
                                        .onFailure(cause -> LOGGER.warn("Failed to create sub route", cause));

                                return vertx.createHttpServer(new HttpServerOptions().setPort(port))
                                        .requestHandler(router)
                                        .listen();
                            })
                            .onSuccess(server -> {
                                LOGGER.info("Server started successfully, listening on port {}", port);
                                promise.complete();
                            })
                            .onFailure(promise::fail);
                })
                .onFailure(promise::fail);

        return promise.future();
    }

    /**
     * Bootstraps all Verticles
     *
     * @return future
     */
    private Future<JsonObject> bootstrapVerticles(JsonObject config) {
        PiveauLogger LOGGER = PiveauLoggerFactory.getLogger(getClass());
        LOGGER.info(config.encodePrettily());

        Promise<JsonObject> promise = Promise.promise();

        PiveauCluster.create(
                vertx,
                ConfigHelper.forConfig(config).forceJsonObject(Constants.ENV_PIVEAU_CLUSTER_CONFIG)).onComplete(cr -> {

            DeploymentOptions options = new DeploymentOptions().setConfig(config);

            Promise<String> shellPromise = Promise.promise();
            vertx.deployVerticle(ShellVerticle.class.getName(), options, shellPromise);

            Promise<String> indexPromise = Promise.promise();
            vertx.deployVerticle(IndexServiceVerticle.class.getName(), options, indexPromise);

            Promise<String> datasetsPromise = Promise.promise();
            vertx.deployVerticle(DatasetsServiceVerticle.class.getName(), options, datasetsPromise);

            Promise<String> distributionsPromise = Promise.promise();
            vertx.deployVerticle(DistributionsServiceVerticle.class.getName(), options, distributionsPromise);

            Promise<String> metricPromise = Promise.promise();
            vertx.deployVerticle(MetricsServiceVerticle.class.getName(), options, metricPromise);

            Promise<String> cataloguesPromise = Promise.promise();
            vertx.deployVerticle(CataloguesServiceVerticle.class.getName(), options, cataloguesPromise);

            Promise<String> translationPromise = Promise.promise();
            vertx.deployVerticle(TranslationServiceVerticle.class.getName(), options, translationPromise);

//            Promise<String> vocabulariesPromise = Promise.promise();
//            vertx.deployVerticle(VocabulariesServiceVerticle.class.getName(), options, vocabulariesPromise);

            Promise<String> keycloakServicePromise = Promise.promise();
            vertx.deployVerticle(KeyCloakServiceVerticle.class.getName(), options, keycloakServicePromise);

            Promise<String> queriesServicePromise = Promise.promise();
            vertx.deployVerticle(QueriesServiceVerticle.class.getName(), options, queriesServicePromise);

            CompositeFuture.all(Arrays.asList(
                    shellPromise.future(),
                    indexPromise.future(),
                    datasetsPromise.future(),
                    distributionsPromise.future(),
                    metricPromise.future(),
                    cataloguesPromise.future(),
                    translationPromise.future(),
//                    vocabulariesPromise.future(),
                    keycloakServicePromise.future(),
                    queriesServicePromise.future()
            )).onComplete(ar -> {
                if (ar.succeeded()) {
                    datasetHandler = new DatasetHandler(vertx);
                    metricsHandler = new MetricsHandler(vertx);
                    distributionHandler = new DistributionHandler(vertx);
                    catalogueHandler = new CatalogueHandler(vertx);
                    translationHandler = new TranslationHandler(vertx);
                    indexHandler = new IndexHandler(vertx);
//                    vocabularyHandler = new VocabularyHandler(vertx);
                    queriesHandler = new QueriesHandler(vertx);

                    promise.complete(config);
                } else {
                    promise.fail(ar.cause());
                }
            });

        });

        return promise.future();
    }

    /**
     * Creates the health and info endpoint
     *
     * @param context  The routing context
     * @param greeting A fancy greeting message
     */
    private void healthHandler(RoutingContext context, String greeting) {
        JsonObject response = new JsonObject();
        response.put("service", "piveau hub");
        response.put("message", greeting);
        response.put("version", "0.0.1");
        response.put("status", "ok");
        context.response().setStatusCode(200);
        context.response().putHeader("Content-Type", "application/json");
        context.response().end(response.encode());
    }

    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 1);
        params[params.length - 1] = MainVerticle.class.getName();
        Launcher.executeCommand("run", params);
    }

}
