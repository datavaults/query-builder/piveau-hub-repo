package io.piveau.hub.services.vocabularies;

import io.piveau.dcatap.TripleStore;
import io.piveau.json.ConfigHelper;
import io.piveau.hub.util.Constants;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceBinder;

public class VocabulariesServiceVerticle extends AbstractVerticle {

    @Override
    public void start(Promise<Void> startPromise) {
        JsonObject config = ConfigHelper.forConfig(config()).forceJsonObject(Constants.ENV_PIVEAU_TRIPLESTORE_CONFIG);
        TripleStore tripleStore = new TripleStore(vertx, config, null);
        VocabulariesService.create(tripleStore, config(), serviceReady -> {
            if (serviceReady.succeeded()) {
                new ServiceBinder(vertx).setAddress(VocabulariesService.SERVICE_ADDRESS)
                        .register(VocabulariesService.class, serviceReady.result());
                startPromise.complete();
            } else {
                startPromise.fail(serviceReady.cause());
            }
        });
    }
}
