package io.piveau.hub.services.vocabularies;

import io.piveau.dcatap.TripleStore;
import io.piveau.hub.util.Constants;
import io.piveau.utils.JenaUtils;
import io.piveau.utils.PiveauContext;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;
import org.apache.jena.rdf.model.Model;

public class VocabulariesServiceImpl implements VocabulariesService {

    private final PiveauContext serviceContext;
    private final TripleStore tripleStore;

    private final boolean prependXmlDeclaration;

    VocabulariesServiceImpl(TripleStore tripleStore, JsonObject config, Handler<AsyncResult<VocabulariesService>> handler) {
        this.tripleStore = tripleStore;

        serviceContext = new PiveauContext("hub-repo", "VocabulariesService");

        prependXmlDeclaration = config.getBoolean(Constants.ENV_PIVEAU_HUB_XML_DECLARATION, false);

        handler.handle(Future.succeededFuture(this));
    }

    @Override
    public VocabulariesService readVocabulary(String graphUri, String acceptType, Handler<AsyncResult<String>> handler) {
        PiveauContext resourceContext = serviceContext.extend(graphUri);
        resourceContext.log().debug("Accept-Type: {}", acceptType);
        tripleStore.getGraph(graphUri).onSuccess(model -> {
            resourceContext.log().debug("Read success");
            handler.handle(Future.succeededFuture(JenaUtils.write(model, acceptType, prependXmlDeclaration)));
        }).onFailure(cause -> {
            if (cause.getMessage().equals("Not Found")) {
                resourceContext.log().debug("Read failed: " + cause.getMessage());
                handler.handle(ServiceException.fail(404, cause.getMessage()));
            } else {
                resourceContext.log().error("Read failed: " + cause.getMessage());
                handler.handle(ServiceException.fail(500, cause.getMessage()));
            }
        });
        return this;
    }

    @Override
    public VocabulariesService createOrUpdateVocabulary(String graphUri, String contentType, String payload,
                                                        Handler<AsyncResult<String>> handler) {
        PiveauContext resourceContext = serviceContext.extend(graphUri);
        resourceContext.log().debug("Content-Type: {}", contentType);
        Model model = JenaUtils.read(payload.getBytes(), contentType);
        tripleStore.postGraph(graphUri, model).onSuccess(result -> {
            resourceContext.log().debug("Put success");
            handler.handle(Future.succeededFuture(result));
        }).onFailure(cause -> {
            resourceContext.log().error("Put failed: " + cause.getMessage());
            handler.handle(ServiceException.fail(500, cause.getMessage()));
        });
        return this;
    }
}
