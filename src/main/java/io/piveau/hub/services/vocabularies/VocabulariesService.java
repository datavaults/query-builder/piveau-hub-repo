package io.piveau.hub.services.vocabularies;

import io.piveau.dcatap.TripleStore;
import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

@ProxyGen
public interface VocabulariesService {
    String SERVICE_ADDRESS = "io.piveau.hub.vocabularies.queue";

    static VocabulariesService create(TripleStore tripleStore, JsonObject config, Handler<AsyncResult<VocabulariesService>> readyHandler) {
        return new VocabulariesServiceImpl(tripleStore, config, readyHandler);
    }

    static VocabulariesService createProxy(Vertx vertx, String address) {
        return new VocabulariesServiceVertxEBProxy(vertx, address);
    }

    @Fluent
    VocabulariesService readVocabulary(String graphUri, String acceptType, Handler<AsyncResult<String>> handler);

    @Fluent
    VocabulariesService createOrUpdateVocabulary(String graphUri, String contentType, String payload,
                                                 Handler<AsyncResult<String>> handler);
}
