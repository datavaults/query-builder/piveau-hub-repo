package io.piveau.hub.services.catalogues;

import io.piveau.dcatap.*;
import io.piveau.hub.security.KeyCloakService;
import io.piveau.hub.services.datasets.DatasetsService;
import io.piveau.hub.services.index.IndexService;
import io.piveau.hub.util.*;
import io.piveau.indexing.Indexing;
import io.piveau.rdf.Piveau;
import io.vertx.core.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CataloguesServiceImpl implements CataloguesService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final CatalogueManager catalogueManager;

    private IndexService indexService;

    private final DatasetsService datasetsService;

    private final KeyCloakService keycloakService;

    CataloguesServiceImpl(TripleStore tripleStore, JsonObject config, Vertx vertx, Handler<AsyncResult<CataloguesService>> readyHandler) {
        catalogueManager = tripleStore.getCatalogueManager();

        if (config.getJsonObject(Constants.ENV_PIVEAU_HUB_INDEX_SERVICE_CONFIG, new JsonObject()).getBoolean("enabled", false)) {
            indexService = IndexService.createProxy(vertx, IndexService.SERVICE_ADDRESS);
        }

        datasetsService = DatasetsService.createProxy(vertx, DatasetsService.SERVICE_ADDRESS);
        keycloakService = KeyCloakService.createProxy(vertx, KeyCloakService.SERVICE_ADDRESS);

        readyHandler.handle(Future.succeededFuture(this));
    }

    /*
     * Returns a list of stripped catalogues
     *
     * Questions:
     * How should we list catalogues?
     * Just a list of id?
     * Stripped?
     */
    @Override
    public CataloguesService listCatalogues(String acceptType, Handler<AsyncResult<String>> handler) {
        if (acceptType.startsWith("application/json")) {
            catalogueManager.listUris()
                    .onSuccess(list -> handler.handle(Future.succeededFuture(new JsonArray(list.stream().map(DCATAPUriRef::getUriRef).toList()).encodePrettily())))
                    .onFailure(cause -> handler.handle(Future.failedFuture(cause)));
        } else {
            catalogueManager.list()
                    .onSuccess(list -> {
                        Model model = ModelFactory.createDefaultModel();
                        list.forEach(model::add);
                        handler.handle(Future.succeededFuture(Piveau.asString(model, acceptType)));
                    })
                    .onFailure(cause -> handler.handle(Future.failedFuture(cause)));
        }
        return this;
    }

    /*
     * Returns a stripped catalogue. No dcat:dataset nor dcat:record entries
     *
     * Questions:
     * Should we change this? If so, do we need some kind of paging?
     */
    @Override
    public CataloguesService getCatalogue(String catalogueId, String acceptType, Handler<AsyncResult<String>> handler) {
        catalogueManager.getStripped(catalogueId)
                .onSuccess(model -> {
                    if (model.isEmpty()) {
                        handler.handle(ServiceException.fail(404, "Catalogue not found"));
                    } else {
                        model.setNsPrefixes(Prefixes.DCATAP_PREFIXES);
                        handler.handle(Future.succeededFuture(Piveau.asString(model, acceptType)));
                    }
                })
                .onFailure(cause -> handler.handle(Future.failedFuture(cause)));
        return this;
    }

    /*
     * Create or update a catalogues metadata.
     *
     * If update, it keeps all dataset and record entries.
     */
    @Override
    public CataloguesService putCatalogue(String catalogueId, String content, String contentType, Handler<AsyncResult<String>> handler) {
        CatalogueHelper catalogueHelper = new CatalogueHelper(catalogueId, contentType, content);
        catalogueManager.exists(catalogueId)
                .compose(exists -> Future.<String>future(promise -> {
                            if (exists) {
                                // update
                                catalogueManager.allDatasets(catalogueId)
                                        .onSuccess(list -> {
                                            list.forEach(dataset -> catalogueHelper.addDataset(dataset.getUriRef()));
                                            promise.complete("updated");
                                        })
                                        .onFailure(promise::fail);
                                catalogueHelper.modified();
                            } else {
                                // create
                                promise.complete("created");
                            }
                        })
                )
                .compose(result -> Future.<String>future(promise ->
                        catalogueManager.setGraph(catalogueHelper.uriRef(), catalogueHelper.getModel())
                                .onSuccess(uriRef -> {
                                    keycloakService.createResource(catalogueHelper.getId());
                                    promise.complete(result);
                                })
                                .onFailure(promise::fail)
                ))
                .onSuccess(result -> {
                    if (indexService != null) {
                        Indexing.indexingCatalogue(catalogueHelper.getModel().getResource(catalogueHelper.uriRef()))
                                .onSuccess(indexMessage -> indexService.addCatalog(indexMessage, ar -> {
                                    if (ar.failed()) {
                                        logger.error("Indexing new catalogue", ar.cause());
                                    }
                                }));
                    }
                    handler.handle(Future.succeededFuture(result));
                })
                .onFailure(cause -> handler.handle(Future.failedFuture(cause)));

        return this;
    }

    /*
     * Deletes a catalogue completely.
     *
     * All datasets, records and metrics are deleted as well.
     */
    @Override
    public CataloguesService deleteCatalogue(String id, Handler<AsyncResult<Void>> handler) {
        catalogueManager.exists(id)
                .onSuccess(exists -> {
                    if (exists) {
                        catalogueManager.allDatasets(id)
                                .onSuccess(list -> {
                                    List<Future<Void>> futures = list.stream()
                                            .map(uriRef -> Future.<Void>future(promise -> datasetsService.deleteDataset(uriRef.getId(), promise)))
                                            .collect(Collectors.toList());
                                    CompositeFuture.join(new ArrayList<>(futures))
                                            .onComplete(ar ->
                                                    catalogueManager.delete(id)
                                                            .onSuccess(v -> {
                                                                keycloakService.deleteResource(id);
                                                                handler.handle(Future.succeededFuture());
                                                            })
                                                            .onFailure(cause -> handler.handle(Future.failedFuture(cause))));
                                })
                                .onFailure(cause -> handler.handle(Future.failedFuture(cause)));
                    } else {
                        handler.handle(ServiceException.fail(404, "Catalogue not found"));
                    }
                })
                .onFailure(cause -> handler.handle(Future.failedFuture(cause)));

        return this;
    }

}
