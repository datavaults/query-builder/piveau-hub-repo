package io.piveau.hub.services.catalogues;

import io.piveau.dcatap.TripleStore;
import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;

@ProxyGen
public interface CataloguesService {
    String SERVICE_ADDRESS="io.piveau.hub.catalogues.queue";

    static CataloguesService create(TripleStore tripleStore, JsonObject config, Vertx vertx, Handler<AsyncResult<CataloguesService>> readyHandler) {
        return new CataloguesServiceImpl(tripleStore, config, vertx, readyHandler);
    }

    static CataloguesService createProxy(Vertx vertx, String address) {
        return new CataloguesServiceVertxEBProxy(vertx, address, new DeliveryOptions().setSendTimeout(120000));
    }

    @Fluent
    CataloguesService listCatalogues(String acceptType, Handler<AsyncResult<String>> handler);

    @Fluent
    CataloguesService getCatalogue(String id, String acceptType, Handler<AsyncResult<String>> handler);

    @Fluent
    CataloguesService putCatalogue(String id, String content, String contentType, Handler<AsyncResult<String>> handler);

    @Fluent
    CataloguesService deleteCatalogue(String id, Handler<AsyncResult<Void>> handler);

}
