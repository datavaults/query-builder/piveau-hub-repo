package io.piveau.hub.services.queries;

import io.piveau.dcatap.TripleStore;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.*;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

@ProxyGen
public interface QueriesService {
    String ADDRESS = "io.piveau.hub.queries.queue";

    static Future<QueriesService> create(TripleStore tripleStore) {
        Promise<QueriesService> promise = Promise.promise();

        new QueriesServiceImpl(tripleStore, promise);

        return promise.future();
    }

    static QueriesService createProxy(Vertx vertx, String address) {
        return new QueriesServiceVertxEBProxy(vertx, address, new DeliveryOptions().setSendTimeout(120000));
    }

    Future<JsonArray> listQueries();

    Future<JsonObject> query(JsonObject query);

}
