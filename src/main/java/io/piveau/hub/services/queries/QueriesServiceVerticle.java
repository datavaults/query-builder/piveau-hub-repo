package io.piveau.hub.services.queries;

import io.piveau.dcatap.TripleStore;
import io.piveau.hub.util.Constants;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceBinder;

public class QueriesServiceVerticle extends AbstractVerticle {
    @Override
    public void start(Promise<Void> startPromise) {
        TripleStore tripleStore = new TripleStore(vertx, config().getJsonObject(Constants.ENV_PIVEAU_TRIPLESTORE_CONFIG, new JsonObject()));
        QueriesService.create(tripleStore)
                .onSuccess(service -> {
                    new ServiceBinder(vertx).setAddress(QueriesService.ADDRESS).register(QueriesService.class, service);
                    startPromise.complete();
                })
                .onFailure(startPromise::fail);

    }
}
