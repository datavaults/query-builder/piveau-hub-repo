package io.piveau.hub.services.queries;

import io.piveau.dcatap.TripleStore;
import io.piveau.queries.QueriesKt;
import io.piveau.queries.QueryExtensionsKt;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.jena.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QueriesServiceImpl implements QueriesService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final TripleStore tripleStore;

    public QueriesServiceImpl(TripleStore tripleStore, Handler<AsyncResult<QueriesService>> readyHandler) {
        this.tripleStore = tripleStore;
        readyHandler.handle(Future.succeededFuture(this));
    }

    @Override
    public Future<JsonArray> listQueries() {
        return Future.succeededFuture(new JsonArray().add("Under development!"));
    }

    @Override
    public Future<JsonObject> query(JsonObject query) {
        return Future.future(promise -> {
            log.info("Querying...");
            Query sparqlQuery = QueriesKt.buildSparql(query);
            log.info("SPARQL: {}", sparqlQuery.serialize());
            if (QueryExtensionsKt.straighten(sparqlQuery).isBlank()) {
                promise.complete(new JsonObject());
            } else {
                tripleStore.select(QueryExtensionsKt.straighten(sparqlQuery))
                        .onSuccess(resultSet -> {
                            log.debug("Query successful");

                            // list of datasets with related catalogues
                            JsonArray result = new JsonArray();
                            resultSet.forEachRemaining(querySolution -> {
                                JsonObject entry = new JsonObject();
                                if (querySolution.contains("dataset")) {
                                    entry.put("dataset", querySolution.getResource("dataset").getURI());
                                }
                                if (querySolution.contains("catalogue")) {
                                    entry.put("catalogue", querySolution.getResource("catalogue").getURI());
                                }
                                if (!entry.isEmpty()) {
                                    result.add(entry);
                                }
                            });
                            promise.complete(new JsonObject().put("result", result));
                        })
                        .onFailure(cause -> {
                            log.error("Select", cause);
                            promise.fail(cause);
                        });
            }
        });
    }

}
