package io.piveau.hub.services.metrics;

import io.piveau.dcatap.DCATAPUriRef;
import io.piveau.dcatap.DCATAPUriSchema;
import io.piveau.dcatap.TripleStore;
import io.piveau.dqv.PiveauMetrics;
import io.piveau.hub.services.index.IndexService;
import io.piveau.hub.util.Constants;
import io.piveau.indexing.Indexing;
import io.piveau.json.ConfigHelper;
import io.piveau.utils.JenaUtils;
import io.piveau.utils.PiveauContext;
import io.piveau.vocabularies.vocabulary.DQV;
import io.piveau.vocabularies.vocabulary.EDP;
import io.vertx.core.*;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.*;
import org.apache.jena.util.ResourceUtils;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.OA;
import org.apache.jena.vocabulary.RDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class MetricsServiceImpl implements MetricsService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final TripleStore tripleStore;
    private final boolean enableHistoricMetrics;
    private final PiveauContext moduleContext = new PiveauContext("hub", "Metrics");

    private IndexService indexService;

    private final boolean prependXmlDeclaration;

    MetricsServiceImpl(TripleStore tripleStore, Vertx vertx, JsonObject config, Handler<AsyncResult<MetricsService>> readyHandler) {
        this.tripleStore = tripleStore;

        JsonObject indexConfig = ConfigHelper.forConfig(config).forceJsonObject(Constants.ENV_PIVEAU_HUB_INDEX_SERVICE_CONFIG);
        if (indexConfig.getBoolean("enabled", false)) {
            indexService = IndexService.createProxy(vertx, IndexService.SERVICE_ADDRESS);
        }

        enableHistoricMetrics = config
                .getJsonObject(Constants.ENV_PIVEAU_HUB_METRICS_CONFIG, new JsonObject())
                .getBoolean("history", false);
        prependXmlDeclaration = config.getBoolean(Constants.ENV_PIVEAU_HUB_XML_DECLARATION, true);
        readyHandler.handle(Future.succeededFuture(this));
    }

    @Override
    public MetricsService getMetrics(String datasetId, String catalogueId, boolean fetchHistoric, String contentType, Handler<AsyncResult<String>> handler) {
        tripleStore.getDatasetManager().identify(datasetId, catalogueId).onSuccess(pair -> {
            DCATAPUriRef schema = DCATAPUriSchema.parseUriRef(pair.getFirst().getURI());
            fetch(schema, fetchHistoric, contentType, handler);
        }).onFailure(cause -> {
            if (cause.getMessage().startsWith("not found")) {
                handler.handle(ServiceException.fail(404, "Dataset for metrics not found"));
            } else {
                handler.handle(Future.failedFuture(cause));
            }
        });
        return this;
    }

    @Override
    public MetricsService getMetricsViaNormalizedID(String normalizedId, boolean fetchHistoric, String contentType, Handler<AsyncResult<String>> handler) {
        DCATAPUriRef schema = DCATAPUriSchema.applyFor(normalizedId);
        fetch(schema, fetchHistoric, contentType, handler);

        return this;
    }

    private void fetch(DCATAPUriRef schema, boolean fetchHistoric, String contentType, Handler<AsyncResult<String>> handler) {
        String graphName = enableHistoricMetrics && fetchHistoric
                ? schema.getHistoricMetricsGraphName()
                : schema.getMetricsGraphName();

        tripleStore.getMetricsManager().getGraph(graphName)
                .onSuccess(model -> {
                    if (model.isEmpty()) {
                        handler.handle(ServiceException.fail(404, "Metrics not found"));
                    } else {
                        String result = JenaUtils.write(model, contentType, prependXmlDeclaration);
                        handler.handle(Future.succeededFuture(result));
                    }
                })
                .onFailure(cause -> handler.handle(Future.failedFuture(cause)));
    }

    @Override
    public MetricsService putMetrics(String datasetUriRef, String content, String contentType, Handler<AsyncResult<String>> handler) {

        DCATAPUriRef datasetSchema = DCATAPUriSchema.parseUriRef(datasetUriRef);

        PiveauContext resourceContext = moduleContext.extend(datasetSchema.getId());

        Dataset dataset = JenaUtils.readDataset(content.getBytes(), contentType);
        List<Model> metrics = PiveauMetrics.listMetricsModels(dataset);

        if (!metrics.isEmpty()) {

            Model dqvModel = metrics.get(0); // We assume there is only one named graph and it is the dqv graph

            // Rename meta object
            dqvModel.listSubjectsWithProperty(RDF.type, DQV.QualityMetadata)
                    .nextOptional()
                    .ifPresent(m -> {
                        m.removeAll(DCTerms.type);
                        m.addProperty(DCTerms.type, EDP.MetricsLatest);
                        ResourceUtils.renameResource(m, datasetSchema.getMetricsUriRef());
                    });

            // replace existing "latest" graph
            Future<String> latestResponse = tripleStore.getMetricsManager().setGraph(datasetSchema.getMetricsGraphName(), dqvModel);

            // link
            String query = "INSERT DATA { GRAPH <" + datasetSchema.getDatasetGraphName() + "> { <" + datasetSchema.getRecordUriRef() + "> <" + DQV.hasQualityMetadata + "> <" + datasetSchema.getMetricsGraphName() + "> } }";
            tripleStore.update(query).onFailure(cause -> log.warn("Reference metrics graph failure", cause));

            if (enableHistoricMetrics) {
                latestResponse.compose(response ->
                        // fetch existing "historic" graph if it exists
                        tripleStore.getMetricsManager()
                                .getGraph(datasetSchema.getHistoricMetricsGraphName())
                                .otherwise(ModelFactory.createDefaultModel())
                ).compose(historyGraph -> {
                    dqvModel.listSubjectsWithProperty(RDF.type, DQV.QualityMetadata)
                            .nextOptional()
                            .ifPresent(m -> {
                                m.removeAll(DCTerms.type);
                                m.addProperty(DCTerms.type, EDP.MetricsHistory);
                                ResourceUtils.renameResource(m, datasetSchema.getHistoricMetricsUriRef());
                            });

                    PiveauMetrics.removeAnnotation(historyGraph, historyGraph.getResource(datasetSchema.getDatasetUriRef()), OA.describing);

                    // add latest metrics to existing "historic" graph
                    return tripleStore.getMetricsManager()
                            .setGraph(datasetSchema.getHistoricMetricsGraphName(), historyGraph.union(dqvModel));
                });
            }

            latestResponse.onSuccess(response -> {
                try {
                    if (indexService != null) {
                        indexScore(datasetSchema.getId(), dqvModel);
                    }
                    handler.handle(Future.succeededFuture(response));
                } catch (Exception e) {
                    handler.handle(ServiceException.fail(500, e.getMessage()));
                }
            }).onFailure(cause -> handler.handle(ServiceException.fail(500, cause.getMessage())));

        } else {
            resourceContext.log().info("No metrics graph found");
            handler.handle(ServiceException.fail(400, "No metrics graph found"));
        }
        return this;
    }

    /**
     * Index the new score/ send it to the hub search
     *
     * @param id      the metrics id
     * @param metrics the model which contains the quality measurement with the score
     */
    private void indexScore(String id, Model metrics) {

        PiveauContext resourceContext = moduleContext.extend(id);

        JsonObject indexedMetrics = Indexing.indexingMetrics(metrics);

        //we need the measurement of score and then the score value, so check, if these conditons are given
        if (indexedMetrics.containsKey("quality_meas") && indexedMetrics.getJsonObject("quality_meas").containsKey("scoring")) {
            JsonObject tmp = indexedMetrics.getJsonObject("quality_meas");
            JsonObject q = new JsonObject().put("quality_meas", new JsonObject().put("scoring", tmp.getInteger("scoring")));
            indexService.modifyDataset(id, q, ar -> {
                if (ar.succeeded()) {
                    resourceContext.log().debug("Updated score in dataset index");
                } else {
                    resourceContext.log().warn("Could not update score in dataset index", ar.cause());
                }
            });
        }
    }

    @Override
    public MetricsService deleteMetrics(String datasetId, String catalogueId, Handler<AsyncResult<Void>> handler) {

        tripleStore.getDatasetManager().identify(datasetId, catalogueId)
                .onSuccess(pair -> {
                    DCATAPUriRef schema = DCATAPUriSchema.parseUriRef(pair.getFirst().getURI());
                    tripleStore.deleteGraph(schema.getMetricsGraphName())
                            .onSuccess(v -> handler.handle(Future.succeededFuture()))
                            .onFailure(cause -> handler.handle(ServiceException.fail(404, cause.getMessage())));
                })
                .onFailure(cause -> handler.handle(ServiceException.fail(500, cause.getMessage())));

        return this;
    }

}
