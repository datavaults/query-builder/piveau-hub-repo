package io.piveau.hub.services.metrics;

import io.piveau.pipe.PipeLauncher;
import io.piveau.dcatap.TripleStore;
import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;

@ProxyGen
public interface MetricsService {
    String SERVICE_ADDRESS = "io.piveau.hub.metrics.queue";

    static MetricsService create(TripleStore tripleStore, Vertx vertx, JsonObject config, Handler<AsyncResult<MetricsService>> readyHandler) {
        return new MetricsServiceImpl(tripleStore, vertx, config, readyHandler);
    }

    static MetricsService createProxy(Vertx vertx, String address) {
        return new io.piveau.hub.services.metrics.MetricsServiceVertxEBProxy(vertx, address, new DeliveryOptions().setSendTimeout(120000));
    }

    /**
     * get a Metrics Graph via the dataset ID and the catalogue ID
     * @param datasetId
     * @param catalogueId
     * @param fetchHistoric
     * @param contentType
     * @param handler
     * @return this
     */
    @Fluent
    MetricsService getMetrics(String datasetId, String catalogueId, boolean fetchHistoric, String contentType, Handler<AsyncResult<String>> handler);

    /**
     * get a Metrics graph via the normalized dataset ID
     *
     * @param normalizedId
     * @param fetchHistoric
     * @param contentType
     * @param handler
     * @return this
     */
    @Fluent
    MetricsService getMetricsViaNormalizedID(String normalizedId, boolean fetchHistoric, String contentType, Handler<AsyncResult<String>> handler);

    @Fluent
    MetricsService putMetrics(String datasetUriRef, String content, String contentType, Handler<AsyncResult<String>> handler);

    @Fluent
    MetricsService deleteMetrics(String datasetId, String catalogueId, Handler<AsyncResult<Void>> handler);

}
