package io.piveau.hub.services.datasets;

import io.piveau.pipe.PipeLauncher;
import io.piveau.dcatap.TripleStore;
import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;

@ProxyGen
public interface DatasetsService {
    String SERVICE_ADDRESS = "io.piveau.hub.datasets.queue";

    static DatasetsService create(TripleStore tripleStore, JsonObject config, PipeLauncher launcher, Vertx vertx, Handler<AsyncResult<DatasetsService>> readyHandler) {
        return new DatasetsServiceImpl(tripleStore, config, launcher, vertx, readyHandler);
    }

    static DatasetsService createProxy(Vertx vertx, String address) {
        return new DatasetsServiceVertxEBProxy(vertx, address, new DeliveryOptions().setSendTimeout(120000));
    }

    @Fluent
    DatasetsService listDatasets(String acceptType, Integer limit, Integer offset, Handler<AsyncResult<String>> handler);

    @Fluent
    DatasetsService listCatalogueDatasets(String acceptType, String catalogueId, Integer limit, Integer offset, Handler<AsyncResult<String>> handler);

    @Fluent
    DatasetsService getDatasetOrigin(String originId, String catalogueId, String acceptType, Handler<AsyncResult<String>> handler);

    @Fluent
    DatasetsService getCatalogRecordOrigin(String originId, String catalogueId, String acceptType, Handler<AsyncResult<String>> handler);

    @Fluent
    DatasetsService getDataset(String id, String acceptType, Handler<AsyncResult<String>> handler);

    @Fluent
    DatasetsService putDataset(String originId, String dataset, String contentType, String catalogueId, Handler<AsyncResult<JsonObject>> handler);

    @Fluent
    DatasetsService postDataset(String content, String contentType, String catalogueId, Handler<AsyncResult<JsonObject>> handler);

    @Fluent
    DatasetsService deleteDatasetOrigin(String originId, String catalogueId, Handler<AsyncResult<Void>> handler);

    @Fluent
    DatasetsService deleteDataset(String id, Handler<AsyncResult<Void>> handler);

    @Fluent
    DatasetsService getRecord(String id, String acceptType, Handler<AsyncResult<String>> handler);

    @Fluent
    DatasetsService indexDataset(String originId, String catalogueId, String defaultLang, Handler<AsyncResult<JsonObject>> handler);

}
