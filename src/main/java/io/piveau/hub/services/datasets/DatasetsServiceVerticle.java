package io.piveau.hub.services.datasets;

import io.piveau.hub.util.Constants;
import io.piveau.pipe.PiveauCluster;
import io.piveau.json.ConfigHelper;
import io.piveau.dcatap.TripleStore;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceBinder;

public class DatasetsServiceVerticle extends AbstractVerticle {

    @Override
    public void start(Promise<Void> startPromise) {
        ConfigHelper configHelper = ConfigHelper.forConfig(config());
        JsonObject conf = configHelper.forceJsonObject(Constants.ENV_PIVEAU_TRIPLESTORE_CONFIG);
        JsonObject clusterConfig = configHelper.forceJsonObject(Constants.ENV_PIVEAU_CLUSTER_CONFIG);

        PiveauCluster.create(vertx, clusterConfig).onComplete(init -> {
            if (init.succeeded()) {
                TripleStore tripleStore = new TripleStore(vertx, conf, null);

                DatasetsService.create(tripleStore, config(), init.result().pipeLauncher(vertx), vertx, ready -> {
                    if (ready.succeeded()) {
                        new ServiceBinder(vertx).setAddress(DatasetsService.SERVICE_ADDRESS).register(DatasetsService.class, ready.result());
                        startPromise.complete();
                    } else {
                        startPromise.fail(ready.cause());
                    }
                });
            } else {
                startPromise.fail(init.cause());
            }
        });
    }

}
