package io.piveau.hub.services.datasets;

import io.piveau.dcatap.*;
import io.piveau.hub.dataobjects.DatasetHelper;
import io.piveau.hub.services.index.IndexService;
import io.piveau.hub.services.translation.TranslationService;
import io.piveau.hub.util.*;
import io.piveau.indexing.Indexing;
import io.piveau.json.ConfigHelper;
import io.piveau.pipe.PipeLauncher;
import io.piveau.rdf.Piveau;
import io.piveau.rdf.RDFMimeTypes;
import io.piveau.sparql.SparqlQueryPool;
import io.piveau.utils.*;
import io.piveau.vocabularies.Concept;
import io.piveau.vocabularies.Languages;
import io.piveau.vocabularies.vocabulary.SPDX;
import io.vertx.core.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;
import org.apache.http.HttpHeaders;
import org.apache.jena.query.*;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.DCTerms;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

public class DatasetsServiceImpl implements DatasetsService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final TripleStore tripleStore;
    private final CatalogueManager catalogueManager;
    private final DatasetManager datasetManager;

    private IndexService indexService;
    private TranslationService translationService;

    private final PipeLauncher launcher;

    private final JsonObject metricsConfig;

    private final PiveauContext serviceContext = new PiveauContext("hub-repo", "datasetsService");

    private final Cache<String, JsonObject> cache;

    private final SparqlQueryPool queryPool;

    DatasetsServiceImpl(TripleStore tripleStore, JsonObject config, PipeLauncher launcher, Vertx vertx, Handler<AsyncResult<DatasetsService>> readyHandler) {

        CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
                .withCache("datasetsService", CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, JsonObject.class,
                                ResourcePoolsBuilder.newResourcePoolsBuilder().heap(100, EntryUnit.ENTRIES))
                        .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofHours(12))))
                .build(true);

        cache = cacheManager.getCache("datasetsService", String.class, JsonObject.class);

        this.launcher = launcher;

        this.tripleStore = tripleStore;
        catalogueManager = tripleStore.getCatalogueManager();
        datasetManager = tripleStore.getDatasetManager();

        JsonObject translationConfig = ConfigHelper.forConfig(config).forceJsonObject(Constants.ENV_PIVEAU_TRANSLATION_SERVICE_CONFIG);
        if (translationConfig.getBoolean("enabled", false)) {
            translationService = TranslationService.createProxy(vertx, TranslationService.SERVICE_ADDRESS);
        }
        JsonObject indexConfig = ConfigHelper.forConfig(config).forceJsonObject(Constants.ENV_PIVEAU_HUB_INDEX_SERVICE_CONFIG);
        if (indexConfig.getBoolean("enabled", false)) {
            indexService = IndexService.createProxy(vertx, IndexService.SERVICE_ADDRESS);
        }

        metricsConfig = ConfigHelper.forConfig(config).forceJsonObject(Constants.ENV_PIVEAU_HUB_METRICS_CONFIG);

        queryPool = SparqlQueryPool.Companion.create(vertx, Path.of("sparql"));

        readyHandler.handle(Future.succeededFuture(this));
    }

    @Override
    public DatasetsService listDatasets(String acceptType, Integer limit, Integer offset, Handler<AsyncResult<String>> handler) {
        datasetManager.list(offset, limit)
                .onSuccess(list -> handler.handle(Future.succeededFuture(new JsonArray(list.stream().map(DCATAPUriRef::getUriRef).toList()).encodePrettily())))
                .onFailure(cause -> handler.handle(Future.failedFuture(cause)));
        return this;
    }

    @Override
    public DatasetsService listCatalogueDatasets(String acceptType, String catalogueId, Integer limit, Integer offset, Handler<AsyncResult<String>> handler) {
        catalogueManager.datasets(catalogueId, offset, limit)
                .onSuccess(list -> handler.handle(Future.succeededFuture(new JsonArray(list.stream().map(DCATAPUriRef::getUriRef).toList()).encodePrettily())))
                .onFailure(cause -> handler.handle(Future.failedFuture(cause)));
        return this;
    }

    @Override
    public DatasetsService getDatasetOrigin(String originId, String catalogueId, String acceptType, Handler<AsyncResult<String>> handler) {
        datasetManager.get(originId, catalogueId)
                .onSuccess(model -> {
                    if (model.isEmpty()) {
                        handler.handle(ServiceException.fail(404, "Dataset not found"));
                    } else {
                        model.setNsPrefixes(Prefixes.DCATAP_PREFIXES);
                        handler.handle(Future.succeededFuture(JenaUtils.write(model, acceptType)));
                    }
                })
                .onFailure(cause -> {
                    if (cause.getMessage().startsWith("not found")) {
                        handler.handle(ServiceException.fail(404, "Dataset not found"));
                    } else {
                        handler.handle(Future.failedFuture(cause));
                    }
                });
        return this;
    }

    @Override
    public DatasetsService getCatalogRecordOrigin(String originId, String catalogueId, String acceptType, Handler<AsyncResult<String>> handler) {
        datasetManager.identify(originId, catalogueId)
                .onSuccess(result -> {
                    getCatalogRecordGraph(DCATAPUriSchema.parseUriRef(result.component2().getURI()).getDatasetGraphName(),
                            DCATAPUriSchema.parseUriRef(result.component2().getURI()).getRecordUriRef(),
                            acceptType, handler);
                })
                .onFailure(cause -> {
                    if (cause.getMessage().startsWith("not found")) {
                        handler.handle(ServiceException.fail(404, "Catalog Record not found. Please check your input."));
                    } else {
                        handler.handle(Future.failedFuture(cause));
                    }
                });
        return this;
    }

    private void getCatalogRecordGraph(String graphName, String recordUriRef, String acceptType, Handler<AsyncResult<String>> handler) {
        datasetManager.getGraph(graphName)
                .onSuccess(model -> {
                    if (model.isEmpty()) {
                        handler.handle(ServiceException.fail(404, "Catalog Record not found"));
                    } else {
                        Model record = Piveau.extractAsModel(model.getResource(recordUriRef), model);
                        handler.handle(Future.succeededFuture(JenaUtils.write(record, acceptType)));
                    }
                })
                .onFailure(cause -> {
                    if (cause.getMessage().startsWith("not found")) {
                        handler.handle(ServiceException.fail(404, "Catalog Record not found"));
                    } else {
                        handler.handle(Future.failedFuture(cause));
                    }
                });
    }

    @Override
    public DatasetsService getDataset(String id, String acceptType, Handler<AsyncResult<String>> handler) {
        DCATAPUriRef schema = DCATAPUriSchema.applyFor(id);
        datasetManager.getGraph(schema.getDatasetGraphName())
                .onSuccess(model -> {
                    if (model.isEmpty()) {
                        handler.handle(ServiceException.fail(404, "Dataset not found"));
                    } else {
                        model.setNsPrefixes(Prefixes.DCATAP_PREFIXES);
                        handler.handle(Future.succeededFuture(JenaUtils.write(model, acceptType)));
                    }
                })
                .onFailure(cause -> handler.handle(Future.failedFuture(cause)));
        return this;
    }

    @Override
    public DatasetsService getRecord(String id, String acceptType, Handler<AsyncResult<String>> handler) {
        DCATAPUriRef schema = DCATAPUriSchema.applyFor(id);
        getCatalogRecordGraph(schema.getDatasetGraphName(), schema.getRecordUriRef(), acceptType, handler);
        return this;
    }

    @Override
    public DatasetsService putDataset(String originId, String content, String contentType, String catalogueId, Handler<AsyncResult<JsonObject>> handler) {

        // New strategy:
        // 1. Check syntax (DatasetHelper) and catalogue exists in parallel
        // 2. Fetch hash and check for create or update
        //  a. Skip -> Do nothing
        //  b. Create -> find free id -> create record, add catalogue entry, translation
        //  c. Update -> fetch old graph -> update record, translation
        // 3. Store
        // 4. Index and start metrics pipe

        AtomicReference<DatasetHelper> datasetHelper = new AtomicReference<>();

        Future<DatasetHelper> datasetHelperFuture = DatasetHelper.create(originId, content, contentType, catalogueId);

        Promise<JsonObject> catalogueExistsPromise = Promise.promise();

        JsonObject catalogueInfo = cache.get(catalogueId);
        if (catalogueInfo == null) {
            DCATAPUriRef catalogueSchema = DCATAPUriSchema.createFor(catalogueId);
            String query = queryPool.format("identifyCatalogue", catalogueSchema.getCatalogueGraphName(), catalogueSchema.getCatalogueUriRef());
            tripleStore.select(query)
                    .onSuccess(resultSet -> {
                        if (resultSet.hasNext()) {
                            JsonObject result = new JsonObject();
                            QuerySolution solution = resultSet.next();
                            if (solution.contains("lang")) {
                                Concept concept = Languages.INSTANCE.getConcept(solution.getResource("lang"));
                                if (concept != null) {
                                    String langCode = Languages.INSTANCE.iso6391Code(concept);
                                    if (langCode == null) {
                                        langCode = Languages.INSTANCE.tedCode(concept).toLowerCase();
                                    }
                                    result.put("langCode", langCode);
                                }
                            }
                            if (solution.contains("type")) {
                                result.put("type", solution.getLiteral("type").getLexicalForm());
                            }
                            cache.putIfAbsent(catalogueId, result);
                            catalogueExistsPromise.complete(result);
                        } else {
                            catalogueExistsPromise.fail(new ServiceException(404, "Catalogue not found"));
                        }
                    })
                    .onFailure(catalogueExistsPromise::fail);
        } else {
            catalogueExistsPromise.complete(catalogueInfo);
        }

        CompositeFuture.all(datasetHelperFuture, catalogueExistsPromise.future()).compose(f -> {
            logger.debug("catalogue checked and dataset read");

            Promise<JsonObject> hashPromise = Promise.promise();

            datasetHelper.set(datasetHelperFuture.result());
            JsonObject catalogue = catalogueExistsPromise.future().result();

            datasetHelper.get().sourceLang(catalogue.getString("langCode"));
            datasetHelper.get().sourceType(catalogue.getString("type"));

            getHash(datasetHelper.get(), hashPromise);
            return hashPromise.future();
        }).compose(hashResult -> {
            Promise<DatasetHelper> promise = Promise.promise();

            String currentHash = datasetHelper.get().hash();
            if (!hashResult.getBoolean("success")) {
                // create
                logger.debug("no hash, creating");
                createDataset(datasetHelper.get()).onComplete(promise);
            } else if (!currentHash.isBlank() && hashResult.getString("hash").equals(currentHash)) {
                // skip
                logger.debug("hash equal, skipping");
                promise.fail(new ServiceException(304, "Dataset is up to date"));
            } else {
                // update
                if (hashResult.getString("hash").isBlank() || currentHash.isBlank()) {
                    logger.warn("At least one hash is blank, updating");
                } else {
                    logger.debug("Hash not equal, updating");
                }
                updateDataset(datasetHelper.get(), hashResult.getString("recordUriRef")).onComplete(promise);
            }
            return promise.future();
        }).compose(finalHelper -> {
            datasetHelper.set(finalHelper);
            return store(datasetHelper.get());
        }).onSuccess(status -> {
            if (indexService != null) {
                index(datasetHelper.get());
            }
            if (metricsConfig.getBoolean("enabled", false)) {
                systemPipes(datasetHelper.get());
            }
            handler.handle(Future.succeededFuture(status));
        }).onFailure(cause -> {
            if (cause instanceof ServiceException) {
                handler.handle(Future.failedFuture(cause));
            } else {
                handler.handle(ServiceException.fail(500, cause.getMessage()));
            }
        });
        return this;
    }

    @Override
    public DatasetsService postDataset(String content, String contentType, String catalogueId, Handler<AsyncResult<JsonObject>> handler) {
        return putDataset(UUID.randomUUID().toString(), content, contentType, catalogueId, handler);
    }

    @Override
    public DatasetsService deleteDataset(String id, Handler<AsyncResult<Void>> handler) {
        delete(DCATAPUriSchema.applyFor(id), handler);
        return this;
    }

    @Override
    public DatasetsService deleteDatasetOrigin(String originId, String catalogueId, Handler<AsyncResult<Void>> handler) {
        datasetManager.identify(originId, catalogueId)
                .onSuccess(pair -> delete(DCATAPUriSchema.parseUriRef(pair.getFirst().getURI()), handler))
                .onFailure(cause -> {
                    if (cause.getMessage().startsWith("not found")) {
                        handler.handle(ServiceException.fail(404, "Dataset not found"));
                    } else {
                        handler.handle(Future.failedFuture(cause));
                    }
                });

        return this;
    }

    private void delete(DCATAPUriRef schema, Handler<AsyncResult<Void>> handler) {
        datasetManager.deleteGraph(schema.getDatasetGraphName())
                .compose(v -> datasetManager.catalogue(schema.getDatasetUriRef()))
                .compose(catalogue -> catalogueManager.removeDatasetEntry(catalogue.getURI(), schema.getDatasetUriRef()))
                .onSuccess(v -> {
                    if (indexService != null) {
                        indexService.deleteDataset(schema.getId(), ar -> {
                            if (ar.failed()) {
                                logger.warn("Delete index", ar.cause());
                            }
                        });
                    }

                    tripleStore.deleteGraph(schema.getMetricsGraphName())
                            .onFailure(cause -> logger.warn("Delete metrics graph", cause));
                    tripleStore.deleteGraph(schema.getHistoricMetricsGraphName())
                            .onFailure(cause -> logger.warn("Delete metrics history graph", cause));

                    handler.handle(Future.succeededFuture());
                })
                .onFailure(cause -> {
                    if (cause.getMessage().startsWith("not found")) {
                        handler.handle(ServiceException.fail(404, "Dataset not found"));
                    } else {
                        handler.handle(Future.failedFuture(cause));
                    }
                });
    }

    @Override
    public DatasetsService indexDataset(String originId, String catalogueId, String defaultLang, Handler<AsyncResult<JsonObject>> handler) {
        String contentType = "application/n-triples";
        getDatasetOrigin(originId, catalogueId, contentType, ar -> {
            if (ar.succeeded()) {
                DatasetHelper.create(originId, ar.result(), contentType, catalogueId, dr -> {
                    if (dr.succeeded()) {
                        DatasetHelper helper = dr.result();
                        JsonObject indexObject = Indexing.indexingDataset(helper.resource(), helper.recordResource(), catalogueId, defaultLang);
                        indexService.addDatasetPut(indexObject, ir -> {
                            if (ir.failed()) {
                                handler.handle(Future.failedFuture(ir.cause()));
                            } else {
                                handler.handle(Future.succeededFuture());
                            }
                        });
                    } else {
                        handler.handle(Future.failedFuture(dr.cause()));
                    }
                });
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    private void getHash(DatasetHelper helper, Handler<AsyncResult<JsonObject>> handler) {
        String query = "SELECT ?hash ?record WHERE { GRAPH <" + helper.catalogueGraphName() + "> { <" + helper.catalogueUriRef() + "> <" + DCAT.record + "> ?record } GRAPH ?g { ?record <" + DCTerms.identifier + "> \"" + helper.originId() + "\"; <" + SPDX.checksum + ">/<" + SPDX.checksumValue + "> ?hash } }";
        tripleStore.select(query)
                .onSuccess(resultSet -> {
                    if (resultSet.hasNext()) {
                        logger.debug("Hash available");
                        QuerySolution solution = resultSet.next();
                        handler.handle(Future.succeededFuture(new JsonObject()
                                .put("success", true)
                                .put("hash", solution.getLiteral("hash").getLexicalForm())
                                .put("recordUriRef", solution.getResource("record").getURI())));
                    } else {
                        logger.debug("No old hash available");
                        handler.handle(Future.succeededFuture(new JsonObject().put("success", false)));
                    }
                })
                .onFailure(cause -> {
                    logger.debug("No old hash available");
                    handler.handle(Future.succeededFuture(new JsonObject().put("success", false)));
                });
    }

    private Future<JsonObject> store(DatasetHelper helper) {
        Promise<JsonObject> promise = Promise.promise();
        datasetManager.setGraph(helper.graphName(), helper.model(), false).onComplete(ar -> {
            if (ar.succeeded()) {
                switch (ar.result()) {
                    case "created":
                        promise.complete(new JsonObject()
                                .put("status", "created")
                                .put("id", helper.piveauId())
                                .put("dataset", helper.stringify(Lang.NTRIPLES))
                                .put(HttpHeaders.LOCATION, helper.uriRef()));
                        break;
                    case "updated":
                        promise.complete(new JsonObject().put("status", "updated").put(HttpHeaders.LOCATION, helper.uriRef()));
                        break;
                    default:
                        promise.fail(ar.result());
                }
            } else {
                promise.fail(ar.cause());
            }
        });
        return promise.future();
    }

    private Future<DatasetHelper> index(DatasetHelper helper) {
        Promise<DatasetHelper> datasetIndexed = Promise.promise();
        try {
            JsonObject indexMessage = Indexing.indexingDataset(helper.resource(), helper.recordResource(), helper.catalogueId(), helper.sourceLang());
            indexService.addDatasetPut(indexMessage, ar -> {
                if (ar.succeeded()) {
                    datasetIndexed.complete(helper);
                } else {
                    datasetIndexed.fail(ar.cause());
                }
            });
        } catch (Exception e) {
            datasetIndexed.fail(new ServiceException(500, e.getMessage()));
        }

        return datasetIndexed.future();
    }

    private void systemPipes(DatasetHelper helper) {
        if (launcher.isPipeAvailable(metricsConfig.getString("metricsPipeName", "metrics-complete"))) {
            JsonObject dataInfo = new JsonObject().put("identifier", helper.originId()).put("catalogue", helper.catalogueId()).put("uriRef", helper.uriRef());
            JsonObject configs = new JsonObject().put("validating-shacl", new JsonObject().put("skip", !"dcat-ap".equals(helper.sourceType())));

            Dataset dataset = DatasetFactory.create(helper.model());
            launcher.runPipeWithData(metricsConfig.getString("metricsPipeName", "metrics-complete"), JenaUtils.write(dataset, Lang.TRIG), RDFMimeTypes.TRIG, dataInfo, configs, null);
        }
    }

    private Future<DatasetHelper> createDataset(DatasetHelper datasetHelper) {
        Promise<DatasetHelper> promise = Promise.promise();
        findPiveauId(datasetHelper.piveauId(), 0, ar -> {
            if (ar.succeeded()) {
                datasetHelper.init(ar.result());

                catalogueManager.addDatasetEntry(
                                datasetHelper.catalogueGraphName(),
                                datasetHelper.catalogueUriRef(),
                                datasetHelper.uriRef(),
                                datasetHelper.recordUriRef())
                        .onFailure(cause -> logger.error("Add catalogue entry for {}", datasetHelper.piveauId(), cause));

                if (translationService != null) {
                    Promise<DatasetHelper> translationPromise = Promise.promise();
                    translationService.initializeTranslationProcess(datasetHelper, null, translationPromise);
                    translationPromise.future()
                            .onSuccess(promise::complete)
                            .onFailure(cause -> promise.complete(datasetHelper));
                } else {
                    promise.complete(datasetHelper);
                }
            } else {
                promise.fail(new ServiceException(500, ar.cause().getMessage()));
            }
        });
        return promise.future();
    }

    private Future<DatasetHelper> updateDataset(DatasetHelper datasetHelper, String recordUriRef) {
        Promise<DatasetHelper> promise = Promise.promise();
        datasetManager.getGraph(DCATAPUriSchema.parseUriRef(recordUriRef).getDatasetGraphName())
                .onSuccess(model -> {
                    datasetHelper.update(model, recordUriRef);

                    if (translationService != null) {
                        DatasetHelper.create(JenaUtils.write(model, Lang.NTRIPLES), Lang.NTRIPLES.getContentType().getContentTypeStr())
                                .onSuccess(oldHelper -> {
                                    Promise<DatasetHelper> translationPromise = Promise.promise();
                                    translationService.initializeTranslationProcess(datasetHelper, oldHelper, translationPromise);
                                    translationPromise.future()
                                            .onSuccess(promise::complete)
                                            .onFailure(cause -> promise.complete(datasetHelper));
                                })
                                .onFailure(cause -> promise.fail(new ServiceException(500, cause.getMessage())));
                    } else {
                        promise.complete(datasetHelper);
                    }
                })
                .onFailure(cause -> promise.fail(new ServiceException(500, cause.getMessage())));

        return promise.future();
    }

    private void findPiveauId(String id, int counter, Handler<AsyncResult<String>> handler) {
        AtomicReference<String> checkId = new AtomicReference<>();
        if (counter > 0) {
            checkId.set(id + "_" + counter);
        } else {
            checkId.set(id);
        }
        tripleStore.ask("ASK WHERE { GRAPH ?catalogue { ?catalogue <" + DCAT.dataset + "> <" + DCATAPUriSchema.applyFor(checkId.get()).getDatasetUriRef() + "> } }")
                .onSuccess(exist -> {
                    if (exist) {
                        findPiveauId(id, counter + 1, handler);
                    } else {
                        handler.handle(Future.succeededFuture(checkId.get()));
                    }
                }).onFailure(cause -> handler.handle(ServiceException.fail(500, cause.getMessage())));
    }

}
