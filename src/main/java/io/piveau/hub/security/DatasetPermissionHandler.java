package io.piveau.hub.security;

import io.piveau.security.PiveauAuth;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;

import java.util.List;

public class DatasetPermissionHandler implements Handler<RoutingContext> {

    private final List<String> scopes;

    public DatasetPermissionHandler(List<String> scopes) {
        this.scopes = scopes;
    }

    @Override
    public void handle(RoutingContext context) {
        String resource = context.queryParams().get("catalogueId");
        if (context.user().principal().containsKey("apiKey")
                || PiveauAuth.userHasRole(context.user(), "operator")
                || scopes.stream().allMatch(scope -> PiveauAuth.userHasPermission(context.user(), resource, scope))) {
            context.next();
        } else {
            context.fail(401);
        }
    }

}
