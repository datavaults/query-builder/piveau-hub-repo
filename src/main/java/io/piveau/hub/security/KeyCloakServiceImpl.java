package io.piveau.hub.security;

import io.piveau.hub.util.Constants;
import io.piveau.security.KeycloakTokenServerConfig;
import io.piveau.security.PiveauAuthConfig;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

public class KeyCloakServiceImpl implements KeyCloakService {

    private KeycloakTokenServerConfig keycloakConfig;

    public KeyCloakServiceImpl(Vertx vertx, JsonObject jsonConfig, Handler<AsyncResult<KeyCloakService>> readyHandler) {
        JsonObject authConfig = jsonConfig.getJsonObject(Constants.ENV_PIVEAU_HUB_AUTH_CONFIG, new JsonObject());
        if (authConfig.getJsonObject("tokenServerConfig", new JsonObject()).containsKey("keycloak")) {
            PiveauAuthConfig config = new PiveauAuthConfig(authConfig);
            if (config.getTokenServerConfig() instanceof KeycloakTokenServerConfig) {
                keycloakConfig = ((KeycloakTokenServerConfig) config.getTokenServerConfig());
            }
        }
        readyHandler.handle(Future.succeededFuture(this));
    }

    @Override
    public KeyCloakService createResource(String catalogueId) {
        return this;
    }

    @Override
    public KeyCloakService deleteResource(String catalogueId) {
        return this;
    }

}
