package io.piveau.hub.handler;

import io.piveau.hub.services.vocabularies.VocabulariesService;
import io.vertx.core.Vertx;
import io.vertx.ext.web.RoutingContext;
import io.vertx.serviceproxy.ServiceException;

import java.util.List;

public class VocabularyHandler {

    private final VocabulariesService vocabulariesService;

    public VocabularyHandler(Vertx vertx) {
        vocabulariesService = VocabulariesService.createProxy(vertx, VocabulariesService.SERVICE_ADDRESS);
    }

    public void createOrUpdateVocabulary(RoutingContext context) {
        String graphUri = context.request().getParam("graphUri");
        String contentType = context.parsedHeaders().contentType().rawValue();
        String payload = context.getBodyAsString();
        List<String> synchronous = context.queryParam("synchronous");
        if (synchronous.isEmpty() || synchronous.contains("true")) {
            vocabulariesService.createOrUpdateVocabulary(graphUri, contentType, payload, ar -> {
                context.response().putHeader("Access-Control-Allow-Origin", "*");
                if (ar.succeeded()) {
                    switch (ar.result()) {
                        case "created":
                            context.response().setStatusCode(201).end();
                            break;
                        case "updated":
                            context.response().setStatusCode(204).end();
                            break;
                        default:
                            // should not happen, succeeded path should only respond with 2xx codes
                            context.response().setStatusCode(400).end();
                    }
                } else {
                    if (ar.cause() instanceof ServiceException) {
                        ServiceException se = (ServiceException) ar.cause();
                        context.response().setStatusCode(se.failureCode()).end(se.getMessage());
                    } else {
                        // should not happen
                        context.response().setStatusCode(500).end(ar.cause().getMessage());
                    }
                }
            });
        } else {
            vocabulariesService.createOrUpdateVocabulary(graphUri, contentType, payload, ar -> {});
            context.response().setStatusCode(202).end();
        }
    }

    public void readVocabulary(RoutingContext context) {
        String graphUri = context.request().getParam("graphUri");
        String acceptType = context.getAcceptableContentType() != null
                ? context.getAcceptableContentType()
                : "application/rdf+xml";
        vocabulariesService.readVocabulary(graphUri, acceptType, ar -> {
            context.response().putHeader("Access-Control-Allow-Origin", "*");
            if (ar.succeeded()) {
                context.response().putHeader("Content-Type", acceptType).end(ar.result());
            } else {
                if (ar.cause() instanceof ServiceException) {
                    ServiceException serviceException = (ServiceException) ar.cause();
                    context.response().putHeader("Content-Type", "text/plain")
                            .setStatusCode(serviceException.failureCode()).end(serviceException.getMessage());
                } else {
                    // should not happen
                    context.response().putHeader("Content-Type", "text/plain")
                            .setStatusCode(500).end(ar.cause().getMessage());
                }
            }
        });
    }
}
