package io.piveau.hub.handler;

import io.piveau.hub.services.translation.TranslationService;
import io.piveau.hub.util.logger.PiveauLoggerFactory;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class TranslationHandler {

    private final TranslationService translationService;

    public TranslationHandler(Vertx vertx) {
        translationService = TranslationService.createProxy(vertx, TranslationService.SERVICE_ADDRESS);
    }

    public void handlePostTranslation(RoutingContext context) {
        JsonObject translation = context.getBodyAsJson();
        PiveauLoggerFactory.getLogger(getClass()).debug(translation.toString());
        translationService.receiveTranslation(translation, ar -> {
            if (ar.succeeded()) {
                JsonObject result = ar.result();
                switch (result.getString("status")) {
                    case "success":
                        context.response().setStatusCode(200).end();
                        break;
                    case "not found":
                        context.response().setStatusCode(404).end();
                        break;
                    default:
                        context.response().setStatusCode(400).end();
                }
            } else {
                context.response().setStatusCode(500).end(ar.cause().getMessage());
            }
        });
    }

}
