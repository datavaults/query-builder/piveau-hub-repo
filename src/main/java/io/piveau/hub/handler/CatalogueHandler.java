package io.piveau.hub.handler;

import io.piveau.hub.services.catalogues.CataloguesService;
import io.piveau.hub.util.ContentNegotiation;
import io.vertx.core.Vertx;
import io.vertx.ext.web.RoutingContext;
import io.vertx.serviceproxy.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CatalogueHandler {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final CataloguesService cataloguesService;

    public CatalogueHandler(Vertx vertx) {
        this.cataloguesService = CataloguesService.createProxy(vertx, CataloguesService.SERVICE_ADDRESS);
    }

    public void handleListCatalogues(RoutingContext context) {
        final String accept = context.getAcceptableContentType() != null ? context.getAcceptableContentType() : "application/json";
        cataloguesService.listCatalogues(accept, ar -> {
            if (ar.succeeded()) {
                context.response().putHeader("Content-Type", accept).end(ar.result());
            } else {
                failureResponse(context, ar.cause());
            }
        });
    }

    public void handleGetCatalogue(RoutingContext context) {
        ContentNegotiation contentNegotiation = ContentNegotiation.byURLSuffix(context);
        String id = contentNegotiation.getId();
        if (id.isBlank()) {
            context.response().setStatusCode(404).end();
            return;
        }
        String acceptType = contentNegotiation.getAcceptType();
        cataloguesService.getCatalogue(id, acceptType, ar -> {
            if (ar.succeeded()) {
                String content = ar.result();
                log.debug(content);
                context.response().putHeader("Content-Type", acceptType).end(content);
            } else {
                failureResponse(context, ar.cause());
            }
        });
    }

    public void handlePutCatalogue(RoutingContext context) {
        String id = context.pathParam("id");
        String contentType = context.parsedHeaders().contentType().value();

        String catalogue = context.getBodyAsString();

        cataloguesService.putCatalogue(id, catalogue, contentType, ar -> {
            if (ar.succeeded()) {
                switch (ar.result()) {
                    case "created":
                        context.response().setStatusCode(201).end();
                        break;
                    case "updated":
                        context.response().setStatusCode(204).end();
                        break;
                    default:
                        // should not happen, succeeded path should only respond with 2xx codes
                        context.response().setStatusCode(500).end();
                }
            } else {
                failureResponse(context, ar.cause());
            }
        });
    }

    public void handleDeleteCatalogue(RoutingContext context) {
        String id = context.pathParam("id");
        cataloguesService.deleteCatalogue(id, ar -> {
            if (ar.succeeded()) {
                context.response().setStatusCode(204).end();
            } else {
                failureResponse(context, ar.cause());
            }
        });
    }

    private void failureResponse(RoutingContext context, Throwable cause) {
        if (cause instanceof ServiceException) {
            ServiceException failure = (ServiceException) cause;
            if (failure.getDebugInfo() == null || failure.getDebugInfo().isEmpty()) {
                context.response().setStatusCode(failure.failureCode()).setStatusMessage(failure.getMessage()).end();
            } else {
                context.response()
                        .setStatusCode(failure.failureCode())
                        .putHeader("Content-Type", "application/json")
                        .end(failure.getDebugInfo().encodePrettily());
            }
        } else {
            context.fail(500, cause);
        }
    }

}
