package io.piveau.hub.handler;

import io.piveau.hub.services.metrics.MetricsService;
import io.piveau.hub.util.ContentNegotiation;
import io.vertx.core.Vertx;
import io.vertx.ext.web.RoutingContext;
import io.vertx.serviceproxy.ServiceException;

public class MetricsHandler {

    private final MetricsService metricsService;

    public MetricsHandler(Vertx vertx) {
        metricsService = MetricsService.createProxy(vertx, MetricsService.SERVICE_ADDRESS);
    }

    public void handleGetMetrics(RoutingContext context) {
        String catalogueId = context.queryParam("catalogue").isEmpty() ? null : context.queryParam("catalogue").get(0);

        boolean normalized = !context.queryParam("useNormalizedID").isEmpty() && !context.queryParam("useNormalizedID").get(0).equals("false");

        if (catalogueId == null && !normalized) {
            context.response().setStatusCode(400).end("parameter `useNormalizedID` or `catalogue` must be used");
            return;
        }

        boolean fetchHistoricGraph = !context.queryParam("historic").isEmpty()
                && Boolean.parseBoolean(context.queryParam("historic").get(0));

        ContentNegotiation contentNegotiation = ContentNegotiation.byURLSuffix(context);
        String id = contentNegotiation.getId();
        String acceptType = contentNegotiation.getAcceptType();

        if (normalized) {
            metricsService.getMetricsViaNormalizedID(id, fetchHistoricGraph, acceptType, ar -> {
                if (ar.succeeded()) {
                    context.response().putHeader("Content-Type", acceptType).end(ar.result());
                } else {
                    failureResponse(context, ar.cause());
                }
            });
        } else {
            metricsService.getMetrics(id, catalogueId, fetchHistoricGraph, acceptType, ar -> {
                if (ar.succeeded()) {
                    context.response().putHeader("Content-Type", acceptType).end(ar.result());
                } else {
                    failureResponse(context, ar.cause());
                }
            });
        }
   }

    public void handlePutMetrics(RoutingContext context) {

        String datasetUriRef = context.queryParams().get("uriRef");
        String contentType = context.parsedHeaders().contentType().value();
        String content = context.getBodyAsString();

        metricsService.putMetrics(datasetUriRef, content, contentType, ar -> {
            if (ar.succeeded()) {
                switch (ar.result()) {
                    case "created":
                        context.response().setStatusCode(201).end();
                        break;
                    case "updated":
                        context.response().setStatusCode(200).end();
                        break;
                    default:
                        context.response().setStatusCode(400).end(ar.result());
                }
            } else {
                if (ar.cause() instanceof ServiceException) {
                    ServiceException ex = (ServiceException) ar.cause();
                    context.response().setStatusCode(ex.failureCode()).end(ex.getMessage());
                } else {
                    context.response().setStatusCode(500).end(ar.cause().getMessage());
                }
            }
        });
    }

    public void handleDeleteMetrics(RoutingContext context) {
        String datasetId = context.pathParam("id");

        if (datasetId == null) {
            datasetId = context.queryParam("id").get(0);
        }

        String catalogueId = context.queryParam("catalogue").get(0);

        metricsService.deleteMetrics(datasetId, catalogueId, ar -> {
            if (ar.succeeded()) {
                context.response().setStatusCode(200).end();
            } else {
                context.response().setStatusCode(500).setStatusMessage(ar.cause().getMessage()).end();
            }
        });
    }

    private void failureResponse(RoutingContext context, Throwable cause) {
        if (cause instanceof ServiceException) {
            ServiceException failure = (ServiceException) cause;
            context.response().setStatusCode(failure.failureCode()).end(failure.getMessage());
        } else {
            context.response().setStatusCode(500).end(cause.getMessage());
        }
    }

}
