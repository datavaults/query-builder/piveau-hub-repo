package io.piveau.hub.handler;

import eu.datavaults.rdf.Asset;
import eu.datavaults.rdf.DataVaults;
import io.piveau.hub.services.datasets.DatasetsService;
import io.piveau.hub.services.distributions.DistributionsService;
import io.piveau.hub.util.Constants;
import io.piveau.hub.util.ContentNegotiation;
import io.piveau.hub.util.logger.PiveauLogger;
import io.piveau.hub.util.logger.PiveauLoggerFactory;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.serviceproxy.ServiceException;
import org.apache.http.HttpHeaders;
import org.apache.jena.riot.RDFFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DatasetHandler {

    private static final String LIMIT = "limit";
    private static final String OFFSET = "offset";

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final DatasetsService datasetsService;
    private final DistributionsService distributionsService;

    public DatasetHandler(Vertx vertx) {
        datasetsService = DatasetsService.createProxy(vertx, DatasetsService.SERVICE_ADDRESS);
        distributionsService = DistributionsService.createProxy(vertx, DistributionsService.SERVICE_ADDRESS);
    }

    public void handleGetDataset(RoutingContext context) {
        ContentNegotiation contentNegotiation = ContentNegotiation.byURLSuffix(context);
        String id = contentNegotiation.getId();
        String acceptType = contentNegotiation.getAcceptType();

        datasetsService.getDataset(id, acceptType, ar -> {
            if (ar.succeeded()) {
                context.response().putHeader(HttpHeaders.CONTENT_TYPE, acceptType).end(ar.result());
            } else {
                failureResponse(context, ar.cause());
            }
        });
    }

    private Handler<AsyncResult<String>> getHandler(RoutingContext context, String acceptType) {
        return ar -> {
            if (ar.succeeded()) {
                context.response().putHeader("Content-Type", acceptType).end(ar.result());
            } else {
                failureResponse(context, ar.cause());
            }
        };
    }

    public void handleGetDatasetRecord(RoutingContext context) {
        ContentNegotiation contentNegotiation = ContentNegotiation.byURLSuffix(context);
        String id = contentNegotiation.getId();
        String acceptType = contentNegotiation.getAcceptType();
        String catalogueId = context.queryParam("catalogue").isEmpty() ? null : context.queryParam("catalogue").get(0);
        boolean normalized = !context.queryParam("useNormalizedID").isEmpty() && !context.queryParam("useNormalizedID").get(0).equals("false");
        if (!normalized && catalogueId == null) {
            context.response().setStatusCode(400).end("Parameter catalogue and useNormalizedID are both missing. Please set one of them.");
            return;
        }
        Handler<AsyncResult<String>> har = getHandler(context, acceptType);
        if (normalized) {
            datasetsService.getRecord(id, acceptType, ar -> {
                if (ar.succeeded()) {
                    context.response().putHeader("Content-Type", acceptType).end(ar.result());
                } else {
                    failureResponse(context, ar.cause());
                }
            });
        } else {
            datasetsService.getCatalogRecordOrigin(id, catalogueId, acceptType, har);
        }

    }

    public void handlePutDatasetNew(RoutingContext context) {
        String id = context.pathParam("id");
        String datasetId = context.pathParam("datasetId");

        String content = context.body().asString();
        String contentType = context.parsedHeaders().contentType().value();

        log.debug("contentType: {}", contentType);
        log.debug("content: {}", content);

        String metadataJsonString = context.queryParams().get("metadataJson");
        if (metadataJsonString != null && "application/ld+json".equalsIgnoreCase(contentType)) {
            Asset asset = DataVaults.buildAsset(datasetId, metadataJsonString, content);
            content = asset.asRdfString(RDFFormat.JSONLD);
        }

        log.debug("final content: {}", content);

        datasetsService.putDataset(datasetId, content, contentType, id, ar -> {
            if (ar.succeeded()) {
                JsonObject status = ar.result();
                switch (status.getString("status")) {
                    case "created":
                        context.response().setStatusCode(201).putHeader(HttpHeaders.LOCATION, ar.result().getString(HttpHeaders.LOCATION, "")).end();
                        break;
                    case "updated":
                        context.response().setStatusCode(204).putHeader(HttpHeaders.LOCATION, ar.result().getString(HttpHeaders.LOCATION, "")).end();
                        break;
                    default:
                        // should not happen, succeeded path should only respond with 2xx codes
                        context.response().setStatusCode(400).end();
                }
            } else {
                failureResponse(context, ar.cause());
            }
        });
    }

    public void handlePutDataset(RoutingContext context) {
        String idParam = context.pathParam("id");
        if (idParam == null) {
            idParam = context.queryParam("id").get(0);
        }
        final String id = idParam;

        String catalogueId = context.queryParam("catalogue").get(0);
        Boolean dataUpload = !context.queryParam("data").isEmpty() && context.queryParam("data").get(0).equals("true");

        String hash = context.queryParam("hash").isEmpty() ? null : context.queryParam("hash").get(0);
        String contentType = context.parsedHeaders().contentType().value();

        String content = context.getBodyAsString();
        datasetsService.putDataset(id, content, contentType, catalogueId, ar -> {
            if (ar.succeeded()) {
                JsonObject status = ar.result();
                switch (status.getString("status")) {
                    case "created":
                        context.response().setStatusCode(201).putHeader(HttpHeaders.LOCATION, ar.result().getString(HttpHeaders.LOCATION, "")).end();
                        break;
                    case "updated":
                        context.response().setStatusCode(204).putHeader(HttpHeaders.LOCATION, ar.result().getString(HttpHeaders.LOCATION, "")).end();
                        break;
                    default:
                        // should not happen, succeeded path should only respond with 2xx codes
                        context.response().setStatusCode(400).end();
                }
            } else {
                failureResponse(context, ar.cause());
            }
        });
    }

    public void handleDeleteDatasetNew(RoutingContext context) {
        String id = context.pathParam("id");

        datasetsService.deleteDataset(id, ar -> {
            if (ar.succeeded()) {
                context.response().setStatusCode(204).end();
            } else {
                failureResponse(context, ar.cause());
            }
        });
    }

    public void handleListCatalogueDatasets(RoutingContext context) {
        String accept = context.getAcceptableContentType();
        String catalogueId = context.pathParam("id");

        Integer limit = context.queryParam(LIMIT).isEmpty() ? 100 : Integer.parseInt(context.queryParam(LIMIT).get(0));
        Integer offset = context.queryParam(OFFSET).isEmpty() ? 0 : Integer.parseInt(context.queryParam(OFFSET).get(0));

        datasetsService.listCatalogueDatasets(accept, catalogueId, limit, offset, ar -> {
            if (ar.succeeded()) {
                context.response().putHeader("Content-Type", accept).end(ar.result());
            } else {
                failureResponse(context, ar.cause());
            }
        });
    }

    public void handleListDatasets(RoutingContext context) {
        String accept = context.getAcceptableContentType();

        Integer limit = context.queryParam(LIMIT).isEmpty() ? 100 : Integer.parseInt(context.queryParam(LIMIT).get(0));
        Integer offset = context.queryParam(OFFSET).isEmpty() ? 0: Integer.parseInt(context.queryParam(OFFSET).get(0));

        datasetsService.listDatasets(accept, limit, offset, ar -> {
            if (ar.succeeded()) {
                context.response().putHeader("Content-Type", "application/json").end(ar.result());
            } else {
                failureResponse(context, ar.cause());
            }
        });
    }

    public void handlePostDatasetDistribution(RoutingContext context) {
        String datasetId = context.queryParam("dataset").get(0);
        String catalogue = !context.queryParam("catalogue").isEmpty() ? context.queryParam("catalogue").get(0) : null;

        PiveauLogger log = PiveauLoggerFactory.getLogger(datasetId, catalogue, getClass());
        boolean normalized = !context.queryParam("useNormalizedID").isEmpty() && !context.queryParam("useNormalizedID").get(0).equals("false");

        String contentType = context.parsedHeaders().contentType().rawValue();
        log.info("received content type: {}", contentType);

        // Content-Type can look like: multipart/form-data; charset=utf-8; boundary=something, (see: https://tools.ietf.org/html/rfc7231#section-3.1.1.1) we need the first part
        String[] contentTypes = contentType.split(";");
        if (contentTypes.length > 0) contentType = contentTypes[0];

        if (!Constants.ALLOWED_CONTENT_TYPES.contains(contentType)) {
            context.response().setStatusCode(400).end("Content-Type header should have one of the following values: " + String.join(", ", Constants.ALLOWED_CONTENT_TYPES));
            return;
        }

        if (!normalized && catalogue == null) {
            context.response().setStatusCode(400).end("Using one of these parameters is required: catalogue, useNormalizedID");
            return;
        }
        String distribution = context.getBodyAsString();
        distributionsService.postDistribution(distribution, datasetId, contentType, catalogue, getPostHandler(context));
        // Handler<AsyncResult<JsonObject>> har = getPostHandler(context);
    }

    private void failureResponse(RoutingContext context, Throwable cause) {
        if (cause instanceof ServiceException) {
            ServiceException failure = (ServiceException) cause;
            context.response().setStatusCode(failure.failureCode()).end(failure.getMessage());
        } else {
            context.fail(500, cause);
        }
    }

    private Handler<AsyncResult<JsonObject>> getPostHandler(RoutingContext context) {
        return ar -> {
            if (ar.succeeded()) {
                JsonObject result = ar.result();
                switch (result.getString("status")) {
                    case "success":
                        String location = result.getString(HttpHeaders.LOCATION, "");
                        if (!location.isEmpty()) {
                            context.response().putHeader(HttpHeaders.LOCATION, location);
                        }
                        context.response().setStatusCode(201).end();
                        break;
                    case "not found":
                        context.response().setStatusCode(404).end();
                        break;
                    case "already exists":
                        context.response().setStatusCode(409).end(result.getString("content", ""));
                        break;
                    default:
                        context.response().setStatusCode(400).end();
                }
            } else {
                context.response().setStatusCode(500).end(ar.cause().getMessage());
            }
        };
    }

}
