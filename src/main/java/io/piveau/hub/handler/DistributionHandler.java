package io.piveau.hub.handler;

import io.piveau.hub.services.distributions.DistributionsService;
import io.piveau.hub.util.Constants;
import io.piveau.hub.util.ContentNegotiation;
import io.piveau.hub.util.logger.PiveauLogger;
import io.piveau.hub.util.logger.PiveauLoggerFactory;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.serviceproxy.ServiceException;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class DistributionHandler {

    private final DistributionsService distributionsService;

    public DistributionHandler(Vertx vertx) {
        distributionsService = DistributionsService.createProxy(vertx, DistributionsService.SERVICE_ADDRESS);
    }

    public void handleGetDistribution(RoutingContext context) {
        ContentNegotiation contentNegotiation = ContentNegotiation.byURLSuffix(context);
        String id = contentNegotiation.getId();
        String acceptType = contentNegotiation.getAcceptType();

        // not Empty and not false -> true
        boolean useIdentifier = !context.queryParam("useIdentifier").isEmpty() && !context.queryParam("useIdentifier").get(0).equals("false");

        if (useIdentifier) {
            distributionsService.getDistributionByIdentifier(id, acceptType, getGetResponseHandler(context, acceptType));
        } else {
            distributionsService.getDistribution(id, acceptType, getGetResponseHandler(context, acceptType));
        }
    }

    private Handler<AsyncResult<String>> getGetResponseHandler(RoutingContext context, String acceptType) {
        return ar -> {
            if (ar.succeeded()) {
                context.response().putHeader("Content-Type", acceptType).end(ar.result());
            } else {
                if (ar.cause() instanceof ServiceException) {
                    ServiceException se = (ServiceException) ar.cause();
                    context.response().setStatusCode(se.failureCode()).end(se.getMessage());
                } else {
                    context.response().setStatusCode(500).end(ar.cause().getMessage());
                }
            }
        };
    }

    public void handlePutDistribution(RoutingContext context) {


        String contentType = context.parsedHeaders().contentType().rawValue();


        // Content-Type can look like: multipart/form-data; charset=utf-8; boundary=something, (see: https://tools.ietf.org/html/rfc7231#section-3.1.1.1) we need the first part
        String[] contentTypes = contentType.split(";");
        if (contentTypes.length > 0) contentType = contentTypes[0];

        if (!Constants.ALLOWED_CONTENT_TYPES.contains(contentType)) {
            context.response().setStatusCode(400).end("Content-Type header should have one of the following values: " + String.join(", ", Constants.ALLOWED_CONTENT_TYPES));

            return;
        }
        String id = URLDecoder.decode(context.pathParam("id"), StandardCharsets.UTF_8);
        String distribution = context.getBodyAsString();
        boolean useIdentifier = !context.queryParam("useIdentifier").isEmpty() && !context.queryParam("useIdentifier").get(0).equals("false");


        if (useIdentifier) {
            distributionsService.putDistributionWithIdentifier(distribution, id, contentType, getPutResponseHandler(context));
        } else {
            distributionsService.putDistribution(distribution, id, contentType, getPutResponseHandler(context));
        }

    }


    private Handler<AsyncResult<JsonObject>> getPutResponseHandler(RoutingContext context) {

        return ar -> {
            if (ar.succeeded()) {
                JsonObject result = ar.result();
                switch (result.getString("status")) {
                    case "success":
                        context.response().setStatusCode(HttpStatus.SC_NO_CONTENT).end();
                        break;
                    case "not found":
                        context.response().setStatusCode(404).end(result.getString("content", ""));
                        break;
                    default:
                        context.response().setStatusCode(400).end();
                }
            } else {
                context.response().setStatusCode(500).end(ar.cause().getMessage());
            }
        };
    }

    public void handleDeleteDistribution(RoutingContext context) {

        String id = context.pathParam("id");

        distributionsService.deleteDistribution(id, ar -> {
            if (ar.succeeded()) {
                JsonObject result = ar.result();
                switch (result.getString("status")) {
                    case "success":
                        context.response().setStatusCode(200).end(result.getString("content", ""));
                        break;
                    case "not found":
                        context.response().setStatusCode(404).end(result.getString("content", ""));
                        break;
                    default:
                        context.response().setStatusCode(400).end();
                }
            } else {
                context.response().setStatusCode(500).end(ar.cause().getMessage() != null ? ar.cause().getMessage() : "Internal Server Error");
            }
        });
    }

}
