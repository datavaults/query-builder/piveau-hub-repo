package io.piveau.hub.handler;

import io.piveau.hub.services.queries.QueriesService;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QueriesHandler {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final QueriesService queriesService;

    public QueriesHandler(Vertx vertx) {
        queriesService = QueriesService.createProxy(vertx, QueriesService.ADDRESS);
    }

    public void listQueries(RoutingContext context) {
        queriesService.listQueries()
                .onSuccess(list -> context.response().end(list.encodePrettily()))
                .onFailure(cause -> context.fail(500, cause));
    }

    public void query(RoutingContext context) {
        JsonObject query = context.getBodyAsJson();
        log.info("Query request: {}", query.encodePrettily());
        queriesService.query(query)
                .onSuccess(result -> context.response().end(result.encodePrettily()))
                .onFailure(cause -> context.fail(500, cause));
    }

}
