package io.piveau.hub.handler;

import io.piveau.hub.services.datasets.DatasetsService;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

public class IndexHandler {

    private final DatasetsService datasetsService;

    public IndexHandler(Vertx vertx) {
        datasetsService = DatasetsService.createProxy(vertx, DatasetsService.SERVICE_ADDRESS);
    }

    public void handleIndexDataset(RoutingContext context) {
        String datasetId = URLDecoder.decode(context.pathParam("id"), StandardCharsets.UTF_8);
        String catalogueId = context.queryParam("catalogue").get(0);
        String defaultLanguage = context.queryParam("language").get(0);

        datasetsService.indexDataset(datasetId, catalogueId, defaultLanguage, ar -> {
            if (ar.succeeded()) {
                context.response().putHeader("Content-Type", "application/json").setStatusCode(200).end();
            } else {
                context.response().
                        putHeader("Content-Type", "application/json")
                        .setStatusCode(500)
                        .end(new JsonObject().put("status", "error").put("message", ar.cause().getMessage()).toString());
            }
        });
    }

}
