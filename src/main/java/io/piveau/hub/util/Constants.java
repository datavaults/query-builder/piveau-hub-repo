package io.piveau.hub.util;

import java.util.List;

final public class Constants {

    static public final String ENV_PIVEAU_TRIPLESTORE_CONFIG = "PIVEAU_TRIPLESTORE_CONFIG";
    static public final String ENV_PIVEAU_HUB_SERVICE_PORT = "PIVEAU_HUB_SERVICE_PORT";
    static public final String ENV_PIVEAU_HUB_AUTH_CONFIG = "PIVEAU_HUB_AUTH_CONFIG";
    static public final String ENV_PIVEAU_HUB_BASE_URI = "PIVEAU_HUB_BASE_URI";
    static public final String ENV_PIVEAU_HUB_METRICS_CONFIG = "PIVEAU_HUB_METRICS_CONFIG";
    static public final String ENV_PIVEAU_HUB_INDEX_SERVICE_CONFIG = "PIVEAU_HUB_SEARCH_SERVICE_CONFIG";
    static public final String ENV_PIVEAU_HUB_XML_DECLARATION = "PIVEAU_HUB_XML_DECLARATION";
    static public final String ENV_PIVEAU_TRANSLATION_SERVICE_CONFIG = "PIVEAU_TRANSLATION_SERVICE_CONFIG";

    static public final String ENV_PIVEAU_HUB_VOCABULARIES_CONFIG = "PIVEAU_HUB_VOCABULARIES_CONFIG";

    static public final String ENV_PIVEAU_HUB_ELASTICSEARCH_ADDRESS = "PIVEAU_HUB_ELASTICSEARCH_ADDRESS";

    static public final String ENV_PIVEAU_HUB_CORS_CONFIG ="PIVEAU_HUB_CORS_CONFIG";

    static public final String ENV_PIVEAU_CLUSTER_CONFIG ="PIVEAU_CLUSTER_CONFIG";

    static public final String ENV_PIVEAU_LOGO_PATH ="PIVEAU_LOGO_PATH";
    static public final String ENV_PIVEAU_FAVICON_PATH ="PIVEAU_FAVICON_PATH";

    public static final List<String> ALLOWED_CONTENT_TYPES = List.of(
            "application/rdf+xml",
            "application/ld+json",
            "text/turtle",
            "text/n3",
            "application/trig",
            "application/n-triples"
    );

    public static final String DEFAULT_API_KEY = "anyKey";
}
