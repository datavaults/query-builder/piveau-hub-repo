package io.piveau.hub.util;

import io.vertx.ext.web.RoutingContext;

public class ContentNegotiation {
    private String id;
    private String acceptType;

    public ContentNegotiation() {
    }

    public ContentNegotiation(String id, String acceptType) {
        this.id = id;
        this.acceptType = acceptType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAcceptType() {
        return acceptType;
    }

    public void setAcceptType(String acceptType) {
        this.acceptType = acceptType;
    }

    public static ContentNegotiation byURLSuffix(RoutingContext context){
        String id = context.pathParam("id");
        String acceptType;

        if (id.endsWith(".jsonld")) {
            acceptType = "application/ld+json";
            id = id.substring(0, id.length() - 7);
        } else if (id.endsWith(".rdf")) {
            acceptType = "application/rdf+xml";
            id = id.substring(0, id.length() - 4);
        } else if (id.endsWith(".n3")) {
            acceptType = "text/n3";
            id = id.substring(0, id.length() - 3);
        } else if (id.endsWith(".ttl")) {
            acceptType = "text/turtle";
            id = id.substring(0, id.length() - 4);
        } else if (id.endsWith(".trig")) {
            acceptType = "application/trig";
            id = id.substring(0, id.length() - 5);
        } else if (id.endsWith(".nt")) {
            acceptType = "application/n-triples";
            id = id.substring(0, id.length() - 3);
        } else {
            acceptType = context.getAcceptableContentType();
            if (acceptType == null || acceptType.isBlank()) {
                acceptType = "application/rdf+xml";
            }
        }

        return new ContentNegotiation(id, acceptType);
    }
}
