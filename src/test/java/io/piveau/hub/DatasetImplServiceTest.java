package io.piveau.hub;

import io.piveau.hub.services.datasets.DatasetsService;
import io.piveau.hub.services.datasets.DatasetsServiceVerticle;
import io.piveau.hub.services.index.IndexService;
import io.piveau.hub.services.metrics.MetricsService;
import io.piveau.hub.services.metrics.MetricsServiceVerticle;
import io.piveau.hub.services.translation.TranslationService;
import io.piveau.hub.util.Constants;
import io.piveau.hub.dataobjects.DatasetHelper;
import io.piveau.rdf.RDFMimeTypes;
import io.piveau.utils.JenaUtils;
import io.piveau.test.MockTripleStore;
import io.piveau.dcatap.DCATAPUriSchema;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import io.vertx.serviceproxy.ServiceException;
import org.apache.http.HttpHeaders;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.RDF;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Testing the datasets service")
@ExtendWith(VertxExtension.class)
class DatasetImplServiceTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(DatasetImplServiceTest.class);
    private static String exampleDataset;
    private final String catalogueID = "test-catalog";
    private DatasetsService datasetsService;
    private MetricsService metricsService;

    @BeforeEach
    void setUp(Vertx vertx, VertxTestContext testContext) {
        DeploymentOptions options = new DeploymentOptions()
                .setWorker(true)
                .setConfig(new JsonObject()
                        .put(Constants.ENV_PIVEAU_TRIPLESTORE_CONFIG, MockTripleStore.getDefaultConfig())
                        .put(Constants.ENV_PIVEAU_HUB_METRICS_CONFIG, new JsonObject().put("host", "localhost").put("enabled", false))
                        .put(Constants.ENV_PIVEAU_TRANSLATION_SERVICE_CONFIG, new JsonObject().put("enabled", false)));

        Checkpoint checkpoint = testContext.checkpoint(4);

        vertx.fileSystem().readFile("example_external_dataset.ttl", readResult -> {
            if (readResult.succeeded()) {
                exampleDataset = readResult.result().toString();
                checkpoint.flag();
            } else {
                testContext.failNow(readResult.cause());
            }
        });

        new MockTripleStore()
                .loadGraph("https://piveau.io/id/catalogue/test-catalog", "example_empty_catalog.ttl")
                .deploy(vertx)
                .onSuccess(v -> checkpoint.flag())
                .onFailure(testContext::failNow);


        vertx.deployVerticle(DatasetsServiceVerticle.class.getName(), options, ar -> {
            if (ar.succeeded()) {
                datasetsService = DatasetsService.createProxy(vertx, DatasetsService.SERVICE_ADDRESS);
                checkpoint.flag();
            } else {
                testContext.failNow(ar.cause());
            }
        });
        vertx.deployVerticle(MetricsServiceVerticle.class.getName(), options, ar -> {
            if (ar.succeeded()) {
                metricsService = MetricsService.createProxy(vertx, MetricsService.SERVICE_ADDRESS);
                checkpoint.flag();
            } else {
                testContext.failNow(ar.cause());
            }
        });

        vertx.eventBus().consumer(IndexService.SERVICE_ADDRESS, message -> {
            message.reply(new JsonObject());
        });
        vertx.eventBus().<JsonObject>consumer(TranslationService.SERVICE_ADDRESS, message -> {
            message.reply(message.body().getJsonObject("helper"));
        });
    }

    @Test
    @DisplayName("Update example dataset")
    void testUpdateExampleDataset(Vertx vertx, VertxTestContext testContext) {
        String datasetID = "update-test-dataset";

        datasetsService.putDataset(datasetID, exampleDataset, RDFMimeTypes.TURTLE, catalogueID, ar -> {
            if (ar.succeeded()) {
                datasetsService.getDatasetOrigin(datasetID, catalogueID, "text/turtle", gr -> {
                    if (gr.succeeded()) {
                        LOGGER.debug(gr.result());
                        datasetsService.putDataset(datasetID, exampleDataset, RDFMimeTypes.TURTLE, catalogueID, pr -> {
                            if (pr.succeeded()) {
                                datasetsService.getDatasetOrigin(datasetID, catalogueID, "text/turtle", gr2 -> {
                                    if (gr2.succeeded()) {
                                        LOGGER.debug(gr2.result());
                                        testContext.completeNow();
                                    } else {
                                        testContext.failNow(gr2.cause());
                                    }
                                });
                            } else {
                                testContext.failNow(pr.cause());
                            }
                        });
                    } else {
                        testContext.failNow(gr.cause());
                    }
                });
            } else {
                testContext.failNow(ar.cause());
            }
        });
    }


    @Test
    @DisplayName("Create an example dataset")
    void testCreateExampleDataset(Vertx vertx, VertxTestContext testContext) {
        String datasetID = "create-test-dataset";

        datasetsService.putDataset(datasetID, exampleDataset, RDFMimeTypes.TURTLE, catalogueID, ar -> {
            if (ar.succeeded()) {
                JsonObject result = ar.result();
                testContext.verify(() -> {
                    assertEquals("created", result.getString("status", ""));
                    String location = result.getString(HttpHeaders.LOCATION);
                    assertNotNull(location);
                    assertEquals(DCATAPUriSchema.applyFor(datasetID).getDatasetUriRef(), location);
                });
                testContext.completeNow();
            } else {
                testContext.failNow(ar.cause());
            }
        });
    }

    @Test
    @DisplayName("Delete an example dataset (inclusively Dqv)")
    void testDeleteExampleDataset(Vertx vertx, VertxTestContext testContext) {
        String datasetID = "delete-test-dataset";

        datasetsService.putDataset(datasetID, exampleDataset, RDFMimeTypes.TURTLE, catalogueID, ar -> {
            if (ar.succeeded()) {
                Buffer buffer = vertx.fileSystem().readFileBlocking("example_metric_1.ttl");
                metricsService.putMetrics("https://piveau.io/set/data/delete-test-dataset", buffer.toString(), RDFMimeTypes.TRIG, mr -> {
                    if (mr.succeeded()) {
                        datasetsService.deleteDatasetOrigin(datasetID, catalogueID, handler -> {
                            if (handler.succeeded()) {
                                datasetsService.getDatasetOrigin(datasetID, catalogueID, RDFMimeTypes.TURTLE, ar2 -> {
                                    if (ar2.failed()) {
                                        testContext.verify(() -> {
                                            assertNotNull(ar2.cause());
                                            assertTrue(ar2.cause() instanceof ServiceException);
                                            assertEquals(404, ((ServiceException) ar2.cause()).failureCode());
                                        });
                                        metricsService.getMetrics(datasetID, catalogueID, false, RDFMimeTypes.TURTLE, ar3 -> {
                                            if (ar3.failed()) {
                                                testContext.verify(() -> {
                                                    assertNotNull(ar3.cause());
                                                    assertTrue(ar3.cause() instanceof ServiceException);
                                                    assertEquals(404, ((ServiceException) ar3.cause()).failureCode());
                                                });
                                                testContext.completeNow();
                                            } else {
                                                testContext.failNow(new Throwable("Metrics still available"));
                                            }
                                        });
                                    } else {
                                        testContext.failNow(new Throwable("Dataset still available"));
                                    }
                                });
                            } else {
                                testContext.failNow(handler.cause());
                            }
                        });
                    } else {
                        testContext.failNow(mr.cause());
                    }
                });
            } else {
                testContext.failNow(ar.cause());
            }
        });
    }

    @Test
    @DisplayName("Receive an example dataset")
    void testGetExampleDataset(Vertx vertx, VertxTestContext testContext) {

        datasetsService.putDataset("get-test-dataset", exampleDataset, RDFMimeTypes.TURTLE, catalogueID, ar -> {
            if (ar.succeeded()) {
                datasetsService.getDatasetOrigin("get-test-dataset", catalogueID, RDFMimeTypes.TURTLE, ar2 -> {
                    if (ar2.succeeded()) {
                        String content = ar2.result();
                        testContext.verify(() -> {
                            assertNotNull(content);
                            DatasetHelper.create(content, RDFMimeTypes.TURTLE, modelResult -> {
                                assertTrue(modelResult.succeeded());
                                assertEquals("get-test-dataset", modelResult.result().piveauId());
                            });
                        });
                        testContext.completeNow();
                    } else {
                        testContext.failNow(ar2.cause());
                    }
                });
            } else {
                testContext.failNow(ar.cause());
            }
        });
    }

    //    @Test
    @DisplayName("Receive an example dataset from normalized id")
    void testGetExampleDatasetNormalizedID(Vertx vertx, VertxTestContext testContext) {
        String datasetID = "test-Get-normalized-Dataset .id";

        datasetsService.putDataset(datasetID, exampleDataset, RDFMimeTypes.TURTLE, catalogueID, ar -> {
            if (ar.succeeded()) {
                datasetsService.getDataset(DCATAPUriSchema.applyFor(datasetID).getId(), RDFMimeTypes.TURTLE, ar2 -> {
                    if (ar2.succeeded()) {
                        String model = ar2.result();
                        testContext.verify(() -> {
                            assertNotNull(model);
                            Model jenaModel = JenaUtils.read(model.getBytes(), RDFMimeTypes.TURTLE);
                            assertNotNull(jenaModel);

                            Resource cat = jenaModel.getResource(DCATAPUriSchema.applyFor(datasetID).getDatasetUriRef());
                            assertNotNull(cat);
                            assertNotNull(cat.getProperty(RDF.type));
                            assertEquals(DCAT.Dataset, cat.getProperty(RDF.type).getObject());
                        });
                        testContext.completeNow();
                    } else {
                        testContext.failNow(ar2.cause());
                    }
                });
            } else {
                testContext.failNow(ar.cause());
            }
        });
    }

    @Test
    @DisplayName("Delete a non existing dataset")
    void testDeleteMissingDataset(Vertx vertx, VertxTestContext testContext) {
        String datasetID = "delete-missing-test-dataset";
        datasetsService.deleteDatasetOrigin(datasetID, catalogueID, ar -> {
            testContext.verify(() -> {
                assertFalse(ar.succeeded());
            });
            testContext.completeNow();
        });
    }

    @Test
    @DisplayName("Counting one dataset")
    void testCountOneDataset(Vertx vertx, VertxTestContext testContext) {
        String datasetID = "count-one-test-dataset";

        datasetsService.putDataset(datasetID, exampleDataset, RDFMimeTypes.TURTLE, catalogueID, ar -> {
            if (ar.succeeded()) {
                datasetsService.listCatalogueDatasets("application/json", catalogueID, 20, 0, ar2 -> {
                    if (ar2.succeeded()) {
                        JsonArray result = new JsonArray(ar2.result());
                        testContext.verify(() -> {
                            assertNotNull(result);
                            assertFalse(result.isEmpty());
                            assertEquals(1, result.size());
                            assertTrue(result.getString(0).endsWith(datasetID));
                        });
                        testContext.completeNow();
                    } else {
                        testContext.failNow(ar2.cause());
                    }
                });
            } else {
                testContext.failNow(ar.cause());
            }
        });
    }

    @Test
    @DisplayName("Counting two datasets")
    void testCountTwoDatasetsInOneCat(Vertx vertx, VertxTestContext testContext) {
        String datasetID = "count-two-test-dataset";

        datasetsService.putDataset(datasetID, exampleDataset, RDFMimeTypes.TURTLE, catalogueID, ar -> {
            if (ar.succeeded()) {
                datasetsService.putDataset(datasetID + "2", exampleDataset, RDFMimeTypes.TURTLE, catalogueID, putHandler -> {
                    if (putHandler.succeeded()) {
                        datasetsService.listCatalogueDatasets("application/json", catalogueID, 20, 0, ar2 -> {
                            if (ar2.succeeded()) {
                                JsonArray result = new JsonArray(ar2.result());
                                testContext.verify(() -> {
                                    assertNotNull(result);
                                    assertFalse(result.isEmpty());
                                    assertEquals(2, result.size());
                                });
                                testContext.completeNow();
                            } else {
                                testContext.failNow(ar2.cause());
                            }
                        });
                    } else {
                        testContext.failNow(putHandler.cause());
                    }
                });
            } else {
                testContext.failNow(ar.cause());
            }
        });
    }

    //TODO: create Dataset & check if distribution is renamed
    //TODO: add update dataset with a dataset that has an additional Dist (without dct:idenifier) & dist is correctly renamed
    //TODO: add update dataset with a dataset that has an additional Dist (with dct:idenifier)  & dist is correctly renamed
    //TODO: list from empty catalogue, list from nonexisting catalog, (list sources, list datasets )x(with&, without catalog)

}
