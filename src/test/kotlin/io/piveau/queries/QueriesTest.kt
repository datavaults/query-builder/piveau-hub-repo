package io.piveau.queries

import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.apache.jena.query.Syntax
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.slf4j.LoggerFactory

@DisplayName("Query builder tests")
@ExtendWith(VertxExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class QueriesTest {
    private val log = LoggerFactory.getLogger(javaClass)

    @Test
    fun `Test empty query`(vertx: Vertx, testContext: VertxTestContext) {

        val sparql = buildSparql(JsonObject())
        testContext.verify {
            assertTrue(sparql.serialize().isBlank())
        }

        val sparql2 = buildSparql(JsonObject().put("userProfile", JsonObject()))
        testContext.verify {
            assertTrue(sparql2.serialize().isBlank())
        }

        testContext.completeNow()
    }

    @Test
    fun `Test dataset query`(vertx: Vertx, testContext: VertxTestContext) {

        val sparql = buildSparql(JsonObject().put("name", "testTitle").put("license", JsonArray().add("CC_BY")))

        log.debug(sparql.toString())

//        testContext.verify {
//            assertEquals(
//                "SELECT ?catalogue ?dataset WHERE { GRAPH ?dataset { ?dataset a <http://www.w3.org/ns/dcat#Dataset> ; <http://purl.org/dc/terms/title> \"testTitle\"@en } GRAPH ?catalogue { ?catalogue a <http://www.w3.org/ns/dcat#Catalog> ; <http://www.w3.org/ns/dcat#dataset> ?dataset } }",
//                sparql.straighten().trim()
//            )
//        }

        testContext.completeNow()
    }

    @Test
    fun `Test catalogue query`(vertx: Vertx, testContext: VertxTestContext) {

        val sparql = buildSparql(
//            JsonObject(
//                """
//                    {
//                        "license": [
//                            "CC_BY",
//                            "CC_BY_SA"
//                            ],
//                        "userProfile": {
//                            "country": [
//                                "Angola",
//                                "Andorra"
//                            ]
//                        }
//                    }"""
//            )
            JsonObject("""
                {
                    "encrypted": true,
                    "userProfile": {
                        "occupation": [
                            2,
                            5
                        ]
                    }
                }
            """.trimIndent())
        )

        log.debug(sparql.toString())

//        testContext.verify {
//            assertEquals(
//                "SELECT ?catalogue ?dataset WHERE { GRAPH ?catalogue { ?catalogue a <http://www.w3.org/ns/dcat#Catalog> ; <http://www.w3.org/ns/dcat#dataset> ?dataset ; <http://purl.org/dc/terms/spatial> ?country VALUES ?country { <http://publications.europa.eu/resource/authority/country/DEU> } } GRAPH ?dataset { } }",
//                sparql.straighten().trim()
//            )
//        }

        testContext.completeNow()
    }

    @Test
    fun `Test some specific queries`(vertx: Vertx, testContext: VertxTestContext) {

        val query = vertx.fileSystem().readFileBlocking("queries/query2.json").toJsonObject()
        val sparql = buildSparql(
            query
//            JsonObject()
//                .put("price", JsonArray()
//                    .add(JsonObject().put("min", 20.0).put("max", 30.0))
//                    .add(JsonObject().put("min", 5.0).put("max", 10.0))
//                    .add(JsonObject().put("min", 100.0).put("max", 200.0))
//                )
//            JsonObject().put("encrypted", true)
        )

        log.debug(sparql.toString())

//        testContext.verify {
//            assertEquals(
//                "SELECT ?catalogue ?dataset WHERE { GRAPH ?catalogue { ?catalogue a <http://www.w3.org/ns/dcat#Catalog> ; <http://www.w3.org/ns/dcat#dataset> ?dataset ; <http://purl.org/dc/terms/spatial> ?country VALUES ?country { <http://publications.europa.eu/resource/authority/country/DEU> } } GRAPH ?dataset { } }",
//                sparql.straighten().trim()
//            )
//        }

        testContext.completeNow()
    }
}