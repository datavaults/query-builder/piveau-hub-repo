# ChangeLog

## Unreleased

## 1.5.8 (2021-06-03)

**Added:**
* Reference to metrics graph in dataset record
* Vocabularies api, service, handler
* Cache for catalogue info
* Verbose output flag for repairing duplicates
* Option to add XML declaration to RDF serializations
* Indexing keyword language

**Fixed:**
* Index all command
* Hash value query
* Delete dataset with normalized id
* Indexing temporal
* Handle hash exceptions

## 1.5.7 (2021-03-09)

**Changed:**
* RDF as default/fallback content-type in ContentNegotiation
* application/n-triples as content-type for n-triples in ContentNegotiation

## 1.5.6 (2021-03-05)

**Changed:**
* ReDoc reference to next 2.x

**Added:**
* Supporting configurable favicon and logo

## 1.5.5 (2021-02-26)
**Changed:**
* Use `RouterBuilder`
* Security schemes in openapi definition

**Added:**
* Add api key handler and provider
* Permission handler for datasets and catalogues
* KeyCloak service for creating and deleting resources

**Removed:**
* APiKey classes, now in piveau-utils

## [1.5.5](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-repo/tags/1.5.5) (2021-02-26)

**Changed:**
* Improved catalogue clearing command

**Fixed:**
* Delete query in repair command

## 1.5.4 (2021-02-26)

**Fixed:**
* Completing fix for repair command error handling

## 1.5.3 (2021-02-26)

**Fixed:**
* Repair command error handling

## 1.5.2 (2021-02-18)

**Added:**
* Configurable logo and favicon
* Logo and favicon for piveau and edp2

**Changed:**
* Eventbus timeout increased to 2 min

**Fixed:**
* Fix algorithm checking for free id

## 1.5.1 (2021-02-03)

**Fixed:**
* Start shell service

## 1.5.0 (2021-02-02)

**Changed:**
* API get record method based on piveau id
* Switched to Vert.x 4.0.0

**Fixed:**
* Finding free id
* Delete dataset
* List datasets
* Put dataset
* Get record
* Get metrics
* Get distribution
* ...

## 1.4.11 (2021-01-04)

**Fixed:**
* Enabled DatasetIndexing test

**Added:**
* Added support for address and telephone in VCARD

## 1.4.10 (2021-01-04)

**Fixed:**
* Exception handling for index shell command

## 1.4.9 (2021-01-04)

**Fixed:**
* Bug in `DatasetHelper` json serialization
* Update old dataset helper from model only

## 1.4.8 (2021-01-04)

**Changed:**
* Complete redesigned sync index shell command

## 1.4.7 (2020-12-23)

**Changed:**
* Improve sync and score shell commands


## 1.4.6 (2020-12-18)

**Added:**
* Missing artifacts `CHANGELOG.md` and `LICENSE.md`
* New shell command `repairDups`
* Completion for `clear` and `repair` shell command

**Changed:**
* OpenAPI including 204 responses
* Refactored `deleteDataset()`
* API versioning alignment
* Add Support for normalizedId for dataset DELETE

## 1.4.5 (2020-11-17)

Era before changelog